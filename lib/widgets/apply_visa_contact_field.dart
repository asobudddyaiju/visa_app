import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class ApplyVisaMobileContactField extends StatefulWidget {

  String label,hint;
  TextEditingController controller;
  ApplyVisaMobileContactField({this.controller,this.hint,this.label});
  @override
  _ApplyVisaMobileContactFieldState createState() => _ApplyVisaMobileContactFieldState();
}

class _ApplyVisaMobileContactFieldState extends State<ApplyVisaMobileContactField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.05),
      height: screenHeight(context, dividedBy: 12),
      // child: TextField(
      //   keyboardType: TextInputType.number,
      //   controller: widget.controller,
      //   decoration: InputDecoration(
      //     labelText: widget.label,
      //     labelStyle: TextStyle(
      //         fontWeight: FontWeight.w500,
      //         fontSize: 17,
      //         fontStyle: FontStyle.normal,
      //         fontFamily: 'Poppins',
      //         color: Constants.kitGradients[4]),
      //     hintText: widget.hint,
      //     hintStyle: TextStyle(
      //         fontWeight: FontWeight.w400,
      //         fontSize: 14,
      //         fontStyle: FontStyle.normal,
      //         fontFamily: 'Poppins',
      //         color: Constants.kitGradients[6]),
      //   ),
      // ),
      child:
      CountryCodePicker(
        onChanged: print,
        hideMainText: true,
        showFlagMain: true,
        showFlag: true,
        initialSelection: 'IN',
        hideSearch: true,
        showCountryOnly: true,
        showOnlyCountryWhenClosed: true,
        alignLeft: true,
      ),
    );
  }
}
