import 'package:flutter/material.dart';
import 'package:flutter_holo_date_picker/date_picker_constants.dart';
import 'package:flutter_holo_date_picker/flutter_holo_date_picker.dart';

class CalendarHolo extends StatefulWidget {
  ValueChanged onChange;
  CalendarHolo({this.onChange});
  @override
  _CalendarHoloState createState() => _CalendarHoloState();
}

class _CalendarHoloState extends State<CalendarHolo> {
  DateTime _selectedDate=DateTime.now();
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return DatePickerWidget(
      looping: false, // default is not looping
      firstDate: DateTime.now(), //DateTime(1960),
      //  lastDate: DateTime(2002, 1, 1),
//              initialDate: DateTime.now(),// DateTime(1994),
      dateFormat:
         "dd-MMMM-yyyy",
         locale: DatePicker.localeFromString('en_us'),
      onChange: (DateTime newDate, _) {
        _selectedDate = newDate;
        widget.onChange(_selectedDate);
        print(_selectedDate);
      },
      pickerTheme: DateTimePickerTheme(
        itemTextStyle: TextStyle(color: Colors.black, fontSize: 12),
        dividerColor: Colors.blue,
      ),

    );

  }
}
