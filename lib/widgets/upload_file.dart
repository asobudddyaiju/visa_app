import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class UploadFiles extends StatefulWidget {
  String title1, title2;

  UploadFiles({this.title1, this.title2});

  @override
  _UploadFilesState createState() => _UploadFilesState();
}

class _UploadFilesState extends State<UploadFiles> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2.3),
      height: screenHeight(context, dividedBy: 8),
      decoration: BoxDecoration(
          color: Constants.kitGradients[7],
          borderRadius: BorderRadius.circular(10)),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              widget.title1,
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 13.0,
                color: Color(0xFF5E5E5E),
                fontWeight: FontWeight.w700,
              ),
            ),
            Image.asset(
              'assets/images/upload_icon.png',
              height: 50,
              width: 50,
            ),
            Text(
              widget.title2,
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 13.0,
                color: Color(0xFF5E5E5E),
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
