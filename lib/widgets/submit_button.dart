import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class SubmitButtonForPayment extends StatefulWidget {
  final String title;
  final Color buttonColor;
  final Color titleColor;
  final Function onTap;
  SubmitButtonForPayment({this.title, this.buttonColor, this.titleColor, this.onTap});
  @override
  _SubmitButtonForPaymentState createState() => _SubmitButtonForPaymentState();
}

class _SubmitButtonForPaymentState extends State<SubmitButtonForPayment> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.1),
      height: screenHeight(context, dividedBy: 15.0),
      child: RaisedButton(
          onPressed: widget.onTap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0)
        ),
        color: widget.buttonColor,
        child: Center(
          child: Text(widget.title, style: TextStyle(
            fontFamily: 'ProximaNova',
            fontSize: 16.0,
            color: widget.titleColor,
            fontWeight: FontWeight.w700,
          ),),
        ),
      )
    );
  }
}
