import 'package:flutter/material.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/back_button.dart';

class ActionAlertBox extends StatefulWidget {
  final Function onPressed;
  final String title;
  final String questionAlert;
  ActionAlertBox({this.onPressed, this.title, this.questionAlert});

  @override
  _ActionAlertBoxState createState() => _ActionAlertBoxState();
}

class _ActionAlertBoxState extends State<ActionAlertBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        insetPadding: EdgeInsets.symmetric(
            vertical: screenHeight(context, dividedBy: 3.6)),
        titlePadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 40),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 40)),
        title: Container(
            height: screenHeight(context, dividedBy: 18),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 100),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 4),
                    ),
                    Text("Redirect to Payment"),
                  ],
                ),
              ],
            )),
        content: Container(
          height: screenHeight(context, dividedBy: 3),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("You will be redirected to our payment page",
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w600)),
              Text("After the completion of your payment,",
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w400)),
              Text("Our executive will contact you",
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w400)),
              Text(
                "Do you wish to Proceed ?",
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              )
            ],
          ),
        ),
        backgroundColor: Colors.transparent,
        actions: [
          Container(
            height: screenHeight(context, dividedBy: 10),
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ButtonAlertBox(
                  title: "Back",
                  onTap: () {
                    Navigator.pop(context);
                  },
                  backButton: true,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 80),
                ),
                ButtonAlertBox(
                  title: "Proceed",
                  onTap: () {
                    widget.onPressed();
                  },
                  backButton: false,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 80),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
