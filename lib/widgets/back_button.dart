import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class ButtonAlertBox extends StatefulWidget {
  String title;
  Function onTap;
  bool backButton;
  ButtonAlertBox({this.backButton, this.title, this.onTap});
  @override
  _ButtonAlertBoxState createState() => _ButtonAlertBoxState();
}

class _ButtonAlertBoxState extends State<ButtonAlertBox> {
  @override
  Widget build(BuildContext context) {
    return widget.backButton == false
        ? Container(
            height: screenHeight(context, dividedBy: 21),
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Constants.kitGradients[3],
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: Offset(-0.4, -0.4),
                  )
                ]),
            child: RaisedButton(
                padding: EdgeInsets.only(),
                onPressed: widget.onTap,
                color: Constants.kitGradients[3],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                    side: BorderSide(
                      color: Constants.kitGradients[3],
                      width: 3,
                    )),
                child: Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 7),
                    ),
                    Text(
                      widget.title,
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                    Icon(Icons.arrow_right_alt_sharp),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 9),
                    ),
                  ],
                )),
          )
        : RaisedButton(
            padding: EdgeInsets.only(),
            onPressed: widget.onTap,
            color: Constants.kitGradients[3],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(
                  color: Constants.kitGradients[3],
                  width: 3,
                )),
            child: Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 9),
                ),
                Text(
                  widget.title,
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 9),
                ),
              ],
            ));
  }
}
