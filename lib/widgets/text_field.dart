import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class TextInField extends StatefulWidget {
  String title;
  String hintText;

  TextInField({this.hintText, this.title});

  @override
  _TextInFieldState createState() => _TextInFieldState();
}

class _TextInFieldState extends State<TextInField> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 80),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1.05),
          child: TextField(
            decoration: InputDecoration(
              labelText: widget.title,
              labelStyle: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Color(0xFF5E5E5E),
                fontWeight: FontWeight.w700,
              ),
              hintText: widget.hintText,
              hintStyle: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 14.0,
                color: Color(0xFFC4C4C4),
                fontWeight: FontWeight.w400,
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[3]),
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[3]),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
