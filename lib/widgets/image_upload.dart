import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/material.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/models/upload_image_request.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/upload_file.dart';

class ImageUpload extends StatefulWidget {
  String title1,title2;
  ValueChanged onChange;
  ImageUpload({this.title1,this.title2,this.onChange});
  @override
  _ImageUploadState createState() => _ImageUploadState();
}

class _ImageUploadState extends State<ImageUpload> {

  AppBloc appBloc = AppBloc();
  final picker = ImagePicker();
  File _image;
  bool uploadStatus=false;


  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
          _image = File(pickedFile.path);
          appBloc.uploadImage(uploadImageRequest:UploadImageRequest(imageFile:_image));

        print('Successfully selected image.' + _image.toString());
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  void initState() {
    appBloc.uploadImageResponse.listen((event) {print("image url response is "+event.file.imageFile.toString());
    widget.onChange(event.file.imageFile);
    setState(() {
      uploadStatus=event.file.imageFile.toString().isNotEmpty?true:false;
    });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  GestureDetector(
      child: _image == null
          ? UploadFiles(
        title1: widget.title1,
        title2: widget.title2,
      )
          : Stack(
        children: [
          Positioned(
            child: Container(
              width: screenWidth(context,
                  dividedBy: 2.3),
              height: screenHeight(context,
                  dividedBy: 8),
              decoration: BoxDecoration(
                  color: Constants.kitGradients[7],
                  borderRadius:
                  BorderRadius.circular(10)),
              child: Image.file(
              _image,
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                  width: screenWidth(context,
                      dividedBy: 10),
                  height: screenHeight(context,
                      dividedBy: 18),
                  child: Icon(Icons.edit,
                      color:
                      Constants.kitGradients[1]))),
          if(uploadStatus==false)Container( width: screenWidth(context,
              dividedBy: 2.2),
              height: screenHeight(context,dividedBy: 7.5),child: Center(child: CircularProgressIndicator()))
        ],
      ),
      onTap: () {
        getImage();
      },
    );
  }
}
