import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class ApplyVisaTextField extends StatefulWidget {
  String label,hint;
  TextEditingController controller;
  ApplyVisaTextField({this.controller,this.hint,this.label});
  @override
  _ApplyVisaTextFieldState createState() => _ApplyVisaTextFieldState();
}

class _ApplyVisaTextFieldState extends State<ApplyVisaTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.05),
      height: screenHeight(context, dividedBy: 12),
      child: TextField(
        controller: widget.controller,
        decoration: InputDecoration(
          labelText: widget.label,
          labelStyle: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 17,
              fontStyle: FontStyle.normal,
              fontFamily: 'Poppins',
              color: Constants.kitGradients[4]),
          hintText: widget.hint,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[3]),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[3]),
          ),
          hintStyle: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14,
              fontStyle: FontStyle.normal,
              fontFamily: 'Poppins',
              color: Constants.kitGradients[6]),
        ),
      ),
    );
  }
}
