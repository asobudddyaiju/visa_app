import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:smart_travel/Auth/auth_service.dart';
import 'package:smart_travel/hive/hive.dart';
import 'package:smart_travel/screens/about_us.dart';
import 'package:smart_travel/screens/check_status.dart';
import 'package:smart_travel/screens/contact_us_2.dart';
import 'package:smart_travel/screens/login.dart';
import 'package:smart_travel/screens/login_page_2.dart';
import 'package:smart_travel/screens/my_visa.dart';
import 'package:smart_travel/screens/news_and_updates.dart';
import 'package:smart_travel/screens/profile.dart';
import 'package:smart_travel/screens/terms_and_conditions.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class HomeDrawer extends StatefulWidget {
  FirebaseAuth auth;

  HomeDrawer({this.auth});

  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  String buttonName = "Sign in";
  String photpurl;
  String displayName;
  String dummydisplayName = 'username';
  bool show_profile;

  @override
  void initState() {
    if (widget.auth.currentUser != null) {
      setState(() {
        show_profile = true;
        buttonName = "Sign Out";
      });
    } else {
      setState(() {
        buttonName = "Sign In";
        displayName = dummydisplayName;
        show_profile = false;
      });
    }
    if ((AppHive().hiveGet(key: 'name') != null) ||
        (AppHive().hiveGet(key: 'photourl') != null)) {
      setState(() {
        photpurl = AppHive().hiveGet(key: 'photourl');
        displayName = AppHive().hiveGet(key: 'name');
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 12),
                      vertical: screenHeight(context, dividedBy: 18)),
                  child: Column(children: [
                    Text(
                      'MY UAE Visa',
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 25.0,
                        color: Color(0xFF016BA6),
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      'Powered by Smart travels',
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Color(0xFF6C6C6C),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ])),
              Container(
                height: 0.5,
                color: Colors.grey,
              ),

              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 12),
                    vertical: screenHeight(context, dividedBy: 20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Profile',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfilePage(auth: widget.auth,)));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'MY UAE Visa',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyVisa()));
                      },
                    ),

                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Check Visa ',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Checkstatus()));
                      },
                    ),
                     GestureDetector(
                       child: Container(
                         color: Colors.transparent,
                         height: screenHeight(context, dividedBy: 20),
                         width: screenWidth(context, dividedBy: 1.5),
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.start,
                           crossAxisAlignment: CrossAxisAlignment.center,
                           children: [
                             Text(
                               'Terms & Condition',
                               style: TextStyle(
                                 fontFamily: 'ProximaNova',
                                 fontSize: 14.0,
                                 color: Color(0xFF5E5E5E),
                                 fontWeight: FontWeight.w400,
                               ),
                             ),
                           ],
                         ),
                       ),
                      onTap: () {
                         Navigator.push(
                             context,
                             MaterialPageRoute(
                                 builder: (context) => TermsConditions()));
                       },
                     ),
                     // GestureDetector(
                     //   child: Container(
                     //     color: Colors.transparent,
                     //     width: screenWidth(context, dividedBy: 1.5),
                     //     height: screenHeight(context, dividedBy: 20),
                     //     child: Row(
                     //       mainAxisAlignment: MainAxisAlignment.start,
                     //       crossAxisAlignment: CrossAxisAlignment.center,
                     //       children: [
                     //         Text(
                     //           'News and update',
                     //           style: TextStyle(
                     //             fontFamily: 'ProximaNova',
                     //             fontSize: 14.0,
                     //             color: Color(0xFF5E5E5E),
                     //             fontWeight: FontWeight.w400,
                     //          ),
                     //         ),
                     //       ],
                     //    ),
                     //   ),
                     //   onTap: () {
                     //     Navigator.push(
                     //         context,
                     //         MaterialPageRoute(
                     //             builder: (context) => NewsUpdates()));
                     //   },
                     // ),

                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'About us',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => AboutUs()));
                      },
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Contact us',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ContactUsNew()));
                      },
                    ),
                    Container(
                      color: Colors.transparent,
                      width: screenWidth(context, dividedBy: 1.5),
                      height: screenHeight(context, dividedBy: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Language(English)',
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              fontSize: 14.0,
                              color: Color(0xFF5E5E5E),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      child: Container(
                        color: Colors.transparent,
                        width: screenWidth(context, dividedBy: 1.5),
                        height: screenHeight(context, dividedBy: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              buttonName,
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 14.0,
                                color: Color(0xFF5E5E5E),
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        if (widget.auth.currentUser == null) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LoginPage2(auth: widget.auth)));
                        } else {
                          void signout() async {
                            await Auth(auth: widget.auth).signOut();
                            Hive.box(Constants.BOX_NAME).clear();
                          }

                          signout();

                          setState(() {
                            buttonName = "Sign in";
                            displayName = dummydisplayName;
                          });
                        }
                      },
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
