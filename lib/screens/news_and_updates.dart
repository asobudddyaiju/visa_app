import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class NewsUpdates extends StatefulWidget {
  @override
  _NewsUpdatesState createState() => _NewsUpdatesState();
}

class _NewsUpdatesState extends State<NewsUpdates> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: screenWidth(context,dividedBy: 7),),
            Text(
              'News & Updates',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: ListView.builder(
          controller: _controller,
          shrinkWrap: true,
          padding: EdgeInsets.only(
              left: screenWidth(context, dividedBy: 40),
              right: screenWidth(context, dividedBy: 40)),
          scrollDirection: Axis.vertical,
          itemCount: 25,
          itemBuilder: (BuildContext contxt, int index) {
            return Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              child: Container(
                width: screenWidth(context, dividedBy: 1),
                // height: screenHeight(context, dividedBy: 6),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          '28 Dec 2020 From 12:20 am',
                          style: TextStyle(
                              color: Constants.kitGradients[6],
                              fontSize: 12,
                              fontFamily: 'ProximaNova',
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          'Welcome',
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: 14,
                              fontFamily: 'ProximaNova',
                              fontWeight: FontWeight.w700),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: Text(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                              color: Color(0xFF5E5E5E),
                              fontSize: 12,
                              fontFamily: 'ProximaNova',
                              fontWeight: FontWeight.w400),
                        )),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Divider(
                      thickness: 2,
                      color: Constants.kitGradients[3],
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }
}
