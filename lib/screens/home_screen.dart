import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/models/visa_list_model.dart';
import 'package:smart_travel/screens/my_uae_visa.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/home_drawer.dart';
import 'package:data_connection_checker/data_connection_checker.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  ScrollController _controller = new ScrollController();
  AppBloc appBloc = new AppBloc();
  FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void initState() {
    checkDataConnection();
    appBloc.visaList();
    super.initState();
  }

  @override
  void didChangeDependencies() async {
    await FlutterStatusbarManager.setHidden(false);
    await FlutterStatusbarManager.setColor(Constants.kitGradients[8]);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _globalKey,
      drawer: HomeDrawer(
        auth: _auth,
      ),
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[3],
        toolbarHeight: screenHeight(context, dividedBy: 15),
        leading: GestureDetector(
          child: Container(
              color: Constants.kitGradients[3],
              height: screenHeight(context, dividedBy: 15),
              child: Icon(Icons.menu)),
          onTap: () {
            _globalKey.currentState.openDrawer();
          },
        ),
      ),
      backgroundColor: Constants.kitGradients[5],
      body: SingleChildScrollView(
        child: StreamBuilder<VisaListResponse>(
            stream: appBloc.visaResponse,
            builder: (context, snapshot) {
              return snapshot.hasData
                  ? ListView.builder(
                      controller: _controller,
                      shrinkWrap: true,
                      padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 40),
                          right: screenWidth(context, dividedBy: 40)),
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.visas.length,
                      itemBuilder: (BuildContext cntxt, int index) {
                        return Padding(
                          padding: EdgeInsets.only(
                              top: screenHeight(context, dividedBy: 40)),
                          child: GestureDetector(
                            child: Container(
                              width: screenWidth(context, dividedBy: 1.6),
                              decoration: BoxDecoration(
                                color: Constants.kitGradients[0],
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Row(
                                children: [
                                  Container(
                                      child: Padding(
                                        padding: EdgeInsets.symmetric(
                                            vertical: screenHeight(context,
                                                dividedBy: 28),
                                            horizontal: screenWidth(context,
                                                dividedBy: 35)),
                                        child: Image.asset(
                                            'assets/images/uae_icon.png'),
                                      ),
                                      width:
                                          screenWidth(context, dividedBy: 5.8),
                                      height:
                                          screenHeight(context, dividedBy: 5.0),
                                      decoration: BoxDecoration(
                                        color: Constants.kitGradients[3],
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            bottomLeft: Radius.circular(10)),
                                      )),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: screenHeight(context,
                                            dividedBy: 70),
                                        horizontal: screenWidth(context,
                                            dividedBy: 35)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: screenWidth(context,
                                              dividedBy: 1.4),
                                          child: Text(
                                            snapshot.data.visas[index].title,
                                            overflow: TextOverflow.visible,
                                            style: TextStyle(
                                              fontFamily: 'ProximaNova',
                                              fontSize: 18.0,
                                              color: Constants.kitGradients[3],
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 180)),
                                        Container(
                                          width: screenWidth(context,
                                              dividedBy: 1.4),
                                          child: Text(
                                            snapshot
                                                .data.visas[index].smallDisc,
                                            overflow: TextOverflow.visible,
                                            style: TextStyle(
                                              fontFamily: 'ProximaNova',
                                              fontSize: 14.0,
                                              color: Constants.kitGradients[2],
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 180)),
                                        Container(
                                          width: screenWidth(context,
                                              dividedBy: 1.4),
                                          child: Text(
                                            '',
                                            // snapshot.data.visas[index].largeDisc,
                                            overflow: TextOverflow.visible,
                                            style: TextStyle(
                                              fontFamily: 'ProximaNova',
                                              fontSize: 14.0,
                                              color: Constants.kitGradients[2],
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                            height: screenHeight(context,
                                                dividedBy: 32)),
                                        Container(
                                          width: screenWidth(context,
                                              dividedBy: 1.4),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              Container(
                                                width: screenWidth(context,
                                                    dividedBy: 4.0),
                                                height: screenHeight(context,
                                                    dividedBy: 35),
                                                decoration: BoxDecoration(
                                                    color: Color(0xFF016BA6),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            18)),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                      snapshot.data.visas[index]
                                                          .price,
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'ProximaNova',
                                                        fontSize: 16.0,
                                                        color: Constants
                                                            .kitGradients[1],
                                                        fontWeight:
                                                            FontWeight.w700,
                                                      ),
                                                    ),
                                                    Text(
                                                      ' AED',
                                                      style: TextStyle(
                                                        fontFamily:
                                                            'ProximaNova',
                                                        fontSize: 16.0,
                                                        color: Constants
                                                            .kitGradients[1],
                                                        fontWeight:
                                                            FontWeight.w400,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MyUAEVisa(
                                            title: snapshot
                                                .data.visas[index].title,
                                            largeDesciption: snapshot
                                                .data.visas[index].largeDisc,
                                            id: snapshot.data.visas[index].id,
                                            price: snapshot
                                                .data.visas[index].price,
                                            description: snapshot
                                                .data.visas[index].smallDisc,
                                          )));
                            },
                          ),
                        );
                      },
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 900)),
                      child: Container(
                          height: screenHeight(context, dividedBy: 1),
                          width: screenWidth(context, dividedBy: 1),
                          child: Column(
                            children: [
                              Expanded(
                                child: Shimmer.fromColors(
                                  baseColor: Colors.grey[300],
                                  highlightColor: Colors.grey[200],
                                  enabled: true,
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                      top: screenHeight(context, dividedBy: 20),
                                    ),
                                    child: ListView.builder(
                                      itemBuilder: (_, __) => Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 8.0),
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              left: screenWidth(context,
                                                  dividedBy: 40)),
                                          child: Row(
                                            children: [
                                              Container(
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical:
                                                                screenHeight(
                                                                    context,
                                                                    dividedBy:
                                                                        28),
                                                            horizontal:
                                                                screenWidth(
                                                                    context,
                                                                    dividedBy:
                                                                        35)),
                                                  ),
                                                  width: screenWidth(context,
                                                      dividedBy: 5.8),
                                                  height: screenHeight(context,
                                                      dividedBy: 5.0),
                                                  decoration: BoxDecoration(
                                                    color: Constants
                                                        .kitGradients[3],
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topLeft: Radius
                                                                .circular(10),
                                                            bottomLeft:
                                                                Radius.circular(
                                                                    10)),
                                                  )),
                                              Padding(
                                                padding: EdgeInsets.symmetric(
                                                    vertical: screenHeight(
                                                        context,
                                                        dividedBy: 70),
                                                    horizontal: screenWidth(
                                                        context,
                                                        dividedBy: 35)),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 1.4),
                                                      child: Text(
                                                        '',
                                                        overflow: TextOverflow
                                                            .visible,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'ProximaNova',
                                                          fontSize: 18.0,
                                                          color: Constants
                                                              .kitGradients[3],
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 180)),
                                                    Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 1.4),
                                                      child: Text(
                                                        '',
                                                        overflow: TextOverflow
                                                            .visible,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'ProximaNova',
                                                          fontSize: 14.0,
                                                          color: Constants
                                                              .kitGradients[2],
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 180)),
                                                    Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 1.4),
                                                      child: Text(
                                                        '',
                                                        // snapshot.data.visas[index].largeDisc,
                                                        overflow: TextOverflow
                                                            .visible,
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'ProximaNova',
                                                          fontSize: 14.0,
                                                          color: Constants
                                                              .kitGradients[2],
                                                          fontWeight:
                                                              FontWeight.w400,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 32)),
                                                    Container(
                                                      width: screenWidth(
                                                          context,
                                                          dividedBy: 1.4),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .end,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .end,
                                                        children: [
                                                          Container(
                                                            width: screenWidth(
                                                                context,
                                                                dividedBy: 4.0),
                                                            height:
                                                                screenHeight(
                                                                    context,
                                                                    dividedBy:
                                                                        35),
                                                            decoration: BoxDecoration(
                                                                color: Color(
                                                                    0xFF016BA6),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            18)),
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Text(
                                                                  '',
                                                                  style:
                                                                      TextStyle(
                                                                    fontFamily:
                                                                        'ProximaNova',
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Constants
                                                                        .kitGradients[1],
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w700,
                                                                  ),
                                                                ),
                                                                Text(
                                                                  ' AED',
                                                                  style:
                                                                      TextStyle(
                                                                    fontFamily:
                                                                        'ProximaNova',
                                                                    fontSize:
                                                                        16.0,
                                                                    color: Constants
                                                                        .kitGradients[1],
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400,
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      itemCount: 8,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                    );
            }),
      ),
    );
  }

  checkDataConnection() async {
    bool status = await DataConnectionChecker().hasConnection;
    if (status == true) {
      print('internet connection ok');
    } else {
      final snack = SnackBar(
        content: Text('No internet connection'),
        duration: Duration(seconds: 3),
      );
      _globalKey.currentState.showSnackBar(snack);
    }
  }
}
