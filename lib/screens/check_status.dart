import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/models/status_request.dart';
import 'package:smart_travel/screens/application_status.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/apply_visa_text_field.dart';

import 'home_screen.dart';

class Checkstatus extends StatefulWidget {
  @override
  _CheckstatusState createState() => _CheckstatusState();
}

class _CheckstatusState extends State<Checkstatus> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  TextEditingController appIdText = TextEditingController();
  AppBloc appBloc = AppBloc();
  bool availability;
  int status;
  bool progressBar = false;

  @override
  void initState() {
    appBloc.statusCheckResponse.listen((event) async {
      setState(() {
        progressBar = false;
        availability = event.available;
      });
      if (event.available == true) {
        status = event.visa.status;
      } else {
        status = 0;
      }
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  ApplicationStatus(available: availability, status: status)));
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 5.0),
            ),
            Text(
              'Check status',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: progressBar == false
          ? Stack(
              children: [
                Positioned(
                  top: 0,
                  child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: screenHeight(context, dividedBy: 15),
                          horizontal: screenWidth(context, dividedBy: 15)),
                      child: Column(children: [
                        Text(
                          'My UAE Visa',
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 25.0,
                            color: Constants.kitGradients[3],
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          'powered by smart travels',
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 12.0,
                            color: Constants.kitGradients[2],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ])),
                ),
                Positioned(
                  top: screenHeight(context, dividedBy: 6.5),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        vertical: screenHeight(context, dividedBy: 65),
                        horizontal: screenWidth(context, dividedBy: 15)),
                    child: Text(
                      'To check and view your visa status, Please order number here',
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[2],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: screenHeight(context, dividedBy: 5.0),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: screenHeight(context, dividedBy: 32),
                          horizontal: screenWidth(context, dividedBy: 15)),
                      child: ApplyVisaTextField(
                        hint: 'Enter Application number',
                        controller: appIdText,
                        label: 'Order Number',
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                          width: screenWidth(context, dividedBy: 1),
                          child: Image.asset(
                            'assets/images/check_status_icon.png',
                            fit: BoxFit.fitWidth,
                          )),
                    ],
                  ),
                ),
                Positioned(
                  bottom: screenHeight(context, dividedBy: 2.2),
                  right: screenWidth(context, dividedBy: 3.2),
                  child: GestureDetector(
                    child: Container(
                      width: screenWidth(context, dividedBy: 2.7),
                      height: screenHeight(context, dividedBy: 20),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[3],
                          borderRadius: BorderRadius.circular(21)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Check',
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              fontSize: 16.0,
                              color: Constants.kitGradients[1],
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      if (appIdText.text.isNotEmpty) {
                        setState(() {
                          progressBar = true;
                        });
                        appBloc.statusCheck(
                            statusRequest: StatusRequest(
                                apiKey: "9fef391a-596a-4f04-81d9-e3fa4a5ebfe3",
                                applicationId: appIdText.text));
                      }
                    },
                  ),
                ),
              ],
            )
          : Center(
              child: JumpingDotsProgressIndicator(
                fontSize: 70.0,
                numberOfDots: 5,
                color: Constants.kitGradients[3],
              ),
            ),
    );
  }
}
