import 'dart:async';

import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class ApplicationStatus extends StatefulWidget {
  bool available;
  int status;
  ApplicationStatus({this.available,this.status});
  @override
  _ApplicationStatusState createState() => _ApplicationStatusState();
}

class _ApplicationStatusState extends State<ApplicationStatus> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
  }

  Future<bool> _willpop() async {
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>HomeScreen()), (route) => false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willpop,
      child: Scaffold(
        key: _globalKey,
        backgroundColor: Constants.kitGradients[1],
        appBar: AppBar(
          leading: GestureDetector(
            child: Container(
              width: screenWidth(context, dividedBy: 10),
              color: Constants.kitGradients[3],
              child: Icon(
                Icons.arrow_back_ios,
                color: Constants.kitGradients[1],
                size: 18,
              ),
            ),
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                      (route) => false);
            },
          ),
          toolbarHeight: screenHeight(context, dividedBy: 15),
          backgroundColor: Constants.kitGradients[3],

        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: screenHeight(context,dividedBy: 10),),
            Container(
              width: screenWidth(context,dividedBy: 1.5),
              height: screenHeight(context,dividedBy: 2.5),
              child: Image.asset('assets/images/splash_icon.png',fit: BoxFit.fill,),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                widget.available==false? Text(
                  'Application not placed please enter a valid Application Id',
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 11.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w700,
                  ),
                ):
                widget.status!=null?Text(
                  widget.status==0?'Application under process.':"Application process completed.",
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 16.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w700,
                  ),
                ):Container(),
              ],
            ),
            SizedBox(height: screenHeight(context,dividedBy: 15),),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Text(
            //       widget.applicationId!=null? "Your Application Number - "+widget.applicationId:"",
            //       style: TextStyle(
            //         fontFamily: 'ProximaNova',
            //         fontSize: 16.0,
            //         color: Constants.kitGradients[4],
            //         fontWeight: FontWeight.w700,
            //       ),
            //     ),
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}
