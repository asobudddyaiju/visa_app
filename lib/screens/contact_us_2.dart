import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/models/contact_us_request.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/apply_visa_text_field.dart';
import 'package:smart_travel/widgets/apply_visa_text_field_mobile_no.dart';
import 'package:smart_travel/widgets/text_field.dart';
import 'package:email_validator/email_validator.dart';

import 'home_screen.dart';

class ContactUsNew extends StatefulWidget {
  @override
  _ContactUsNewState createState() => _ContactUsNewState();
}

class _ContactUsNewState extends State<ContactUsNew> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  TextEditingController mobileText = TextEditingController();
  TextEditingController emailText = TextEditingController();
  TextEditingController subjectText = TextEditingController();
  TextEditingController fullNameText = TextEditingController();
  TextEditingController messageText = TextEditingController();
  AppBloc appBloc = AppBloc();
  bool progressStatus = false;

  @override
  void initState() {
    appBloc.contactUsResponse.listen((event) {
      print('Contact us response id is ' + event.file.id.toString());
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomeScreen()),
          (route) => false);
      showToast('message send successfully');
      setState(() {
        progressStatus = false;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 4.8),
            ),
            Text(
              'Contact Us',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: progressStatus == true
          ? Center(
              child: JumpingDotsProgressIndicator(
                fontSize: 70.0,
                numberOfDots: 5,
                color: Constants.kitGradients[3],
              ),
            )
          : Stack(
              children: [
                Positioned(
                  bottom: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 2.1),
                        child: Image.asset('assets/images/contact_us_icon.png',
                            fit: BoxFit.fill),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: screenHeight(context, dividedBy: 18),
                  right: screenWidth(context, dividedBy: 3.2),
                  child: GestureDetector(
                    child: Container(
                      width: screenWidth(context, dividedBy: 2.7),
                      height: screenHeight(context, dividedBy: 20),
                      decoration: BoxDecoration(
                          color: Color(0xFF016BA6),
                          borderRadius: BorderRadius.circular(21)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Submit now',
                            style: TextStyle(
                              fontFamily: 'ProximaNova',
                              fontSize: 16.0,
                              color: Constants.kitGradients[1],
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                      ),
                    ),
                    onTap: () {
                      if (
                          subjectText.text.isNotEmpty &&
                          messageText.text.isNotEmpty)
                           {
                        appBloc.contactUs(
                            contactUsRequest: ContactUsRequest(
                                subject: subjectText.text,
                                message: messageText.text));
                        setState(() {
                          progressStatus = true;
                        });

                      } else {
                        showSnackBar('Enter all fields');
                      }
                    },
                  ),
                ),
                Positioned(
                    top: 2.0,
                    child: Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: ApplyVisaTextField(
                          controller: subjectText,
                          label: 'Subject',
                          hint: 'Enter Subject',
                        ))),
                Positioned(
                    top: screenHeight(context, dividedBy: 8),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Message',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Poppins',
                                color: Constants.kitGradients[4]),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 70),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1.05),
                            height: screenHeight(context, dividedBy: 5),
                            decoration: BoxDecoration(
                                color: Constants.kitGradients[12],
                                border: Border.all(
                                    color: Constants.kitGradients[11],
                                    width: 1),
                                borderRadius: BorderRadius.circular(5.0)),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5.0),
                              child: TextField(
                                maxLines: 5,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'message',
                                  hintStyle: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      fontStyle: FontStyle.normal,
                                      fontFamily: 'Poppins',
                                      color: Constants.kitGradients[13]),
                                ),
                                controller: messageText,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ))
              ],
            ),
    );
  }

  showSnackBar(String status) {
    final snack = SnackBar(
      content: Text(status),
      duration: Duration(seconds: 2),
    );
    _globalKey.currentState.showSnackBar(snack);
  }
}
