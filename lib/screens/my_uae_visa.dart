import 'package:flutter/material.dart';
import 'package:smart_travel/screens/apply_for_visa.dart';
import 'package:smart_travel/screens/contact_us_2.dart';
import 'package:smart_travel/screens/home_screen.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

class MyUAEVisa extends StatefulWidget {
  String title, price, description;
  int id;
  dynamic largeDesciption;
  MyUAEVisa(
      {this.title,
      this.price,
      this.description,
      this.largeDesciption,
      this.id});
  @override
  _MyUAEVisaState createState() => _MyUAEVisaState();
}

class _MyUAEVisaState extends State<MyUAEVisa> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  ScrollController _controller = new ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.largeDesciption.toString());
    return Scaffold(
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Color(0xFF016BA6),
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 5),
            ),
            Text(
              "Visa Details",
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Positioned(
              top: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: Image.asset(
                        'assets/images/my_uae_visa.png',
                        fit: BoxFit.fitWidth,
                      )),
                ],
              )),
          Positioned(
            top: screenHeight(context, dividedBy: 3.6),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 33),
                  vertical: screenHeight(context, dividedBy: 65)),
              child: Row(
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1.4),
                    child: Text(
                      widget.title,
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 18.0,
                        color: Color(0xFF016BA6),
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 35),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 5.2),
                    height: screenHeight(context, dividedBy: 17),
                    decoration: BoxDecoration(
                        color: Color(0xFF016BA6),
                        borderRadius: BorderRadius.circular(18)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          widget.price,
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 16.0,
                            color: Constants.kitGradients[1],
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Text(
                          ' AED',
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 16.0,
                            color: Constants.kitGradients[1],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          Positioned(
            top: screenHeight(context, dividedBy: 2.7),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 30)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 0.3,
                    width: screenWidth(context, dividedBy: 1.06),
                    color: Constants.kitGradients[6],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 28),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'Description',
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          fontFamily: 'ProximaNova',
                          fontSize: 16.0,
                          color: Constants.kitGradients[4],
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.0),
                    child: Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 15)),
                      child: Text(
                        widget.description,
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          fontFamily: 'ProximaNova',
                          fontSize: 12.0,
                          color: Constants.kitGradients[4],
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        // widget.largeDesciption.toString(),
                        "Steps",
                        overflow: TextOverflow.visible,
                        style: TextStyle(
                          fontFamily: 'ProximaNova',
                          fontSize: 16.0,
                          color: Constants.kitGradients[4],
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.1),
                    child: Text(
                      widget.largeDesciption.toString(),
                      // "    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris  nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          // Positioned(
          //   bottom: screenHeight(context, dividedBy: 10.3),
          //   child: Container(
          //     height: screenHeight(context, dividedBy: 10),
          //     width: screenWidth(context, dividedBy: 1),
          //     child: ListView.builder(
          //         scrollDirection: Axis.horizontal,
          //         controller: _controller,
          //         shrinkWrap: true,
          //         itemCount: 10,
          //         padding: EdgeInsets.symmetric(
          //             vertical: screenHeight(context, dividedBy: 50),
          //             horizontal: screenWidth(context, dividedBy: 10)),
          //         itemBuilder: (BuildContext cntxt, int index) {
          //           return Padding(
          //             padding: EdgeInsets.symmetric(
          //                 horizontal: screenWidth(context, dividedBy: 40)),
          //             child: Container(
          //               width: screenWidth(context, dividedBy: 2.5),
          //               height: screenHeight(context, dividedBy: 20),
          //               decoration: BoxDecoration(
          //                   color: Color(0xFF016BA6),
          //                   borderRadius: BorderRadius.circular(18)),
          //               child: Center(
          //                 child: Text(
          //                   'How to apply',
          //                   style: TextStyle(
          //                     fontFamily: 'ProximaNova',
          //                     fontSize: 16.0,
          //                     color: Constants.kitGradients[1],
          //                     fontWeight: FontWeight.w700,
          //                   ),
          //                 ),
          //               ),
          //             ),
          //           );
          //         }),
          //   ),
          // ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                GestureDetector(
                  child: Container(
                    height: screenHeight(context, dividedBy: 12),
                    width: screenWidth(context, dividedBy: 2),
                    color: Constants.kitGradients[3],
                    child: Center(
                      child: Text(
                        'Inquire Now',
                        style: TextStyle(
                          fontFamily: 'ProximaNova',
                          fontSize: 16.0,
                          color: Constants.kitGradients[1],
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ContactUsNew()));
                  },
                ),
                GestureDetector(
                  child: Container(
                    height: screenHeight(context, dividedBy: 12),
                    width: screenWidth(context, dividedBy: 2),
                    color: Constants.kitGradients[3],
                    child: Center(
                      child: Text(
                        'Apply Now',
                        style: TextStyle(
                          fontFamily: 'ProximaNova',
                          fontSize: 16.0,
                          color: Constants.kitGradients[1],
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ApplyForVisa(
                                  id: widget.id,
                                  price: widget.price,
                                )));
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
