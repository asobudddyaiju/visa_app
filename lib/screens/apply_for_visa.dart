import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/hive/hive.dart';
import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:smart_travel/widgets/apply_visa_text_field.dart';
import 'package:smart_travel/widgets/calendar_holo.dart';
import 'package:smart_travel/widgets/country_code_field.dart';
import 'package:smart_travel/widgets/image_upload.dart';
import 'package:smart_travel/widgets/submit_button.dart';
import 'package:url_launcher/url_launcher.dart';

import 'home_screen.dart';

class ApplyForVisa extends StatefulWidget {
  final int id;
  final String price;
  ApplyForVisa({this.id, this.price});
  @override
  _ApplyForVisaState createState() => _ApplyForVisaState();
}

class _ApplyForVisaState extends State<ApplyForVisa> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  DateTime entryDate = DateTime.now();
  // TextEditingController emailText = TextEditingController();
  TextEditingController countryCodeText = TextEditingController();
  TextEditingController nationalityText = TextEditingController();
  TextEditingController contactText = TextEditingController();
  TextEditingController codeText = TextEditingController();
  TextEditingController fullnameText = TextEditingController();
  AppBloc appBloc = AppBloc();
  String url1, url2, url3, url4, url5, applicationId;
  bool loading = false;
  String urlForPayment;

  @override
  void initState() {
    appBloc.applyVisaResponse.listen((event) {
      if (event.status == 200) {
        applicationId = event.file.applicationId;
        // Navigator.pushReplacement(
        //     context,
        //     MaterialPageRoute(
        //         builder: (context) =>
        //             ApplicationPlaced(
        //               applicationId: applicationId,
        //             )));
      }
    });
    appBloc.getPaymentUrlResponse.listen((event) {
      setState(() {
        loading = false;
      });
      if (event.root.statusCode.t == "200") {
        actionAlertBox(
            onPressed: () {
              _launchURL(event.root.result.cdata);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                  (route) => false);
            },
            context: context);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Color(0xFF016BA6),
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 5.2),
            ),
            Text(
              'Apply for Visa',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: loading == true
          ? Center(
              child: JumpingDotsProgressIndicator(
                fontSize: 70.0,
                numberOfDots: 5,
                color: Constants.kitGradients[3],
              ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: SingleChildScrollView(
                child: Container(
                  width: screenWidth(context, dividedBy: 1),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      ApplyVisaTextField(
                        controller: fullnameText,
                        hint: 'Enter Full name',
                        label: 'Full name',
                      ),
                      // ApplyVisaTextField(
                      //   controller: emailText,
                      //   hint: 'Enter Email address',
                      //   label: 'Email',
                      // ),
                      ApplyVisaTextField(
                        controller: countryCodeText,
                        hint: 'Choose code',
                        label: 'Country code',
                      ),
                      Container(
                        // color: Colors.red,
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 12),
                        child: TextField(
                          onTap: () {
                            showAlertDialog(context);
                          },
                          readOnly: true,
                          decoration: InputDecoration(
                            labelText: 'Date of entry',
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Poppins',
                                color: Constants.kitGradients[4]),
                            hintText: entryDate.day.toString() +
                                " - " +
                                entryDate.month.toString() +
                                " - " +
                                entryDate.year.toString(),
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Constants.kitGradients[3]),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide:
                                  BorderSide(color: Constants.kitGradients[3]),
                            ),
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Poppins',
                                color: Constants.kitGradients[4]),
                          ),
                        ),
                      ),
                      ApplyVisaTextField(
                        controller: nationalityText,
                        hint: 'Choose nationality',
                        label: 'Nationality',
                      ),
                      CountryCodeField(
                        controller: codeText,
                        hint: 'Enter Country Calling Code',
                        label: 'Calling Code',
                        showFlag: false,
                        hideSearch: false,
                        showCountryOnly: false,
                        textStyle: TextStyle(wordSpacing: 30),
                        onChanged: (value) {
                          setState(() {
                            codeText.text = value.toString();
                          });
                        },
                      ),
                      ApplyVisaTextField(
                        controller: contactText,
                        hint: 'Enter Contact number',
                        label: 'Contact Number',
                      ),
                      Container(
                          width: screenWidth(context, dividedBy: 1),
                          height: screenHeight(context, dividedBy: 2.3),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  ImageUpload(
                                    title1: "Passport Page 1",
                                    title2: "tap to upload",
                                    onChange: (url) {
                                      setState(() {
                                        url1 = url;
                                      });
                                    },
                                  ),
                                  ImageUpload(
                                    title1: "Passport Page 2",
                                    title2: "tap to upload",
                                    onChange: (url) {
                                      setState(() {
                                        url2 = url;
                                      });
                                    },
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  ImageUpload(
                                    title1: "Passport Page 3",
                                    title2: "tap to upload",
                                    onChange: (url) {
                                      setState(() {
                                        url3 = url;
                                      });
                                    },
                                  ),
                                  ImageUpload(
                                    title1: "Personal photo",
                                    title2: "tap to upload",
                                    onChange: (url) {
                                      url4 = url;
                                    },
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  ImageUpload(
                                    title1: "Ticket copy",
                                    title2: "tap to upload",
                                    onChange: (url) {
                                      url5 = url;
                                    },
                                  ),
                                  Container(
                                    width: screenWidth(context, dividedBy: 2.3),
                                    height: screenHeight(context, dividedBy: 8),
                                  ),
                                ],
                              )
                            ],
                          )),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      SubmitButtonForPayment(
                        title: "Submit",
                        titleColor: Constants.kitGradients[1],
                        buttonColor: Constants.kitGradients[3],
                        onTap: () async {
                          print(AppHive().getUserEmail());
                          if (fullnameText.text.isNotEmpty &&
                              nationalityText.text.isNotEmpty &&
                              countryCodeText.text.isNotEmpty &&
                              contactText.text.isNotEmpty &&
                              codeText.text.isNotEmpty &&
                              url1 != null &&
                              url2 != null &&
                              url3 != null &&
                              url4 != null &&
                              url5 != null) {
                            setState(() {
                              loading = true;
                            });
                            await appBloc.applyVisaList(
                                applyVisaRequest: ApplyVisaRequest(
                                    apiKey:
                                        "9fef391a-596a-4f04-81d9-e3fa4a5ebfe3",
                                    visa: widget.id.toString(),
                                    name: fullnameText.text,
                                    email: AppHive().getUserEmail(),
                                    nationality: nationalityText.text,
                                    countryCode: countryCodeText.text,
                                    dateOfEntry: entryDate.year.toString() +
                                        "-" +
                                        entryDate.month.toString() +
                                        "-" +
                                        entryDate.day.toString(),
                                    contactNumber:
                                        codeText.text + contactText.text,
                                    passportPage1: url1,
                                    passportPage2: url2,
                                    passportPage3: url3,
                                    personalPhoto: url4,
                                    ticketCopy: url5));
                            appBloc.getPaymentUrl(
                                name: fullnameText.text,
                                mobileNo: contactText.text,
                                amount: widget.price);
                            // snackBar('please enter a valid email');
                            // urlForPayment = Urls.paymentBaseUrl + "username=" + ObjectFactory().appHive.getUserName().trim() +
                            //     "&password=" + ObjectFactory().appHive.getUId().trim() +
                            //     "apiauthkey=SMRT78GTR98&opid=TX004&actioncode=ONLINEPAYMENT&"
                            //     "customer_=Maneesh&email=ms@iweensoft.com&mobile=8447968610&"
                            //     "totalamount=100&validtill=2021-02-25";
                            //     _launchURL(urlForPayment);
                          } else {
                            snackBar('enter all fields');
                          }
                        },
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  showAlertDialog(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("Done"),
      onPressed: () {
        if (entryDate == null) {
          entryDate = DateTime.now();
        }
        Navigator.of(context).pop();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Center(
          child: Text(
        "Select Date",
        style: TextStyle(color: Constants.kitGradients[4]),
      )),
      content: CalendarHolo(
        onChange: (newDate) {
          setState(() {
            entryDate = newDate;
          });
          print("via" + newDate.toString());
        },
      ),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  snackBar(String status) {
    final snack = SnackBar(
      content: Text(status.toString()),
      duration: Duration(seconds: 2),
    );
    _globalKey.currentState.showSnackBar(snack);
  }

  _launchURL(String url) async {
    print("url for payment is " + url);
    if (await canLaunch(url)) {
      await launch(
        url,
        enableJavaScript: true,
        forceWebView: false,
        forceSafariVC: false,
      );
    } else {
      throw 'Could not launch $url';
    }
  }
}
