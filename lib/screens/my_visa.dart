import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/models/my_vis_response.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class MyVisa extends StatefulWidget {
  @override
  _MyVisaState createState() => _MyVisaState();
}

class _MyVisaState extends State<MyVisa> {
  ScrollController _controller = new ScrollController();
  AppBloc appBloc = new AppBloc();

  @override
  void initState() {
    appBloc.myVisa();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 4.0),
            ),
            Text(
              'My Visa',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: StreamBuilder<MyVisaResponse>(
          stream: appBloc.myVisaResponse,
          builder: (context, snapshot) {
            return snapshot.hasData
                ? snapshot.data.visas.isNotEmpty?ListView.builder(
                    controller: _controller,
                    shrinkWrap: true,
                    padding: EdgeInsets.only(
                        left: screenWidth(context, dividedBy: 40),
                        right: screenWidth(context, dividedBy: 40)),
                    scrollDirection: Axis.vertical,
                    itemCount: snapshot.data.visas.length,
                    itemBuilder: (BuildContext cntxt, int index) {
                      return Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 40)),
                        child: GestureDetector(
                          child: Container(
                            width: screenWidth(context, dividedBy: 1.6),
                            height: screenHeight(context, dividedBy: 10),
                            decoration: BoxDecoration(
                              color: Constants.kitGradients[0],
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      screenWidth(context, dividedBy: 30)),
                              child: Row(
                                children: [
                                  Container(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Image.asset(
                                          'assets/images/uae_icon.png',
                                        ),
                                      ),
                                      width:
                                          screenWidth(context, dividedBy: 7.8),
                                      height: screenHeight(context,
                                          dividedBy: 15.0),
                                      decoration: BoxDecoration(
                                        color: Constants.kitGradients[3],
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                      )),
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 25),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        width: screenWidth(context,
                                            dividedBy: 1.4),
                                        child: Text(
                                          "Visa change inside country",
                                          overflow: TextOverflow.visible,
                                          style: TextStyle(
                                            fontFamily: 'ProximaNova',
                                            fontSize: 18.0,
                                            color: Constants.kitGradients[3],
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 180)),
                                      Container(
                                        width: screenWidth(context,
                                            dividedBy: 1.4),
                                        child: Row(
                                          children: [snapshot.data.visas[index]
                                              .status ==
                                              0
                                              ?  SvgPicture.asset('assets/images/pending.svg')
                                              : snapshot.data.visas[index]
                                              .status ==
                                              1
                                              ?  SvgPicture.asset('assets/images/pending_blue.svg')
                                              : snapshot
                                              .data
                                              .visas[index]
                                              .status ==
                                              2
                                              ?  SvgPicture.asset('assets/images/processing.svg')
                                              : snapshot
                                              .data
                                              .visas[
                                          index]
                                              .status ==
                                              3
                                              ? SvgPicture.asset('assets/images/done.svg')
                                              : snapshot
                                              .data
                                              .visas[
                                          index]
                                              .status ==
                                              10
                                              ? Icon(Icons.cancel,color: Constants.kitGradients[14],)
                                              : snapshot.data.visas[index]
                                              .status ==
                                              11
                                              ? Icon(Icons.cancel,color: Constants.kitGradients[14],)
                                              :
                                            SvgPicture.asset('assets/images/pending.svg'),
                                            SizedBox(
                                                width: screenWidth(context,
                                                    dividedBy: 40)),
                                            Text(
                                              snapshot.data.visas[index]
                                                          .status ==
                                                      0
                                                  ? "Verification Pending"
                                                  : snapshot.data.visas[index]
                                                              .status ==
                                                          1
                                                      ? "Payment Pending"
                                                      : snapshot
                                                                  .data
                                                                  .visas[index]
                                                                  .status ==
                                                              2
                                                          ? "Visa Processing"
                                                          : snapshot
                                                                      .data
                                                                      .visas[
                                                                          index]
                                                                      .status ==
                                                                  3
                                                              ? "Visa Processed"
                                                              : snapshot
                                                                          .data
                                                                          .visas[
                                                                              index]
                                                                          .status ==
                                                                      10
                                                                  ? "Verification Failed"
                                                                  : snapshot.data.visas[index]
                                                                              .status ==
                                                                          11
                                                                      ? "Visa Declined"
                                                                      : "Unknown",
                                              overflow: TextOverflow.visible,
                                              style: TextStyle(
                                                fontFamily: 'ProximaNova',
                                                fontSize: 14.0,
                                                color:snapshot.data.visas[index]
                                                    .status ==
                                                    0
                                                    ? Constants.kitGradients[16]
                                                    : snapshot.data.visas[index]
                                                    .status ==
                                                    1
                                                    ? Constants.kitGradients[8]
                                                    : snapshot
                                                    .data
                                                    .visas[index]
                                                    .status ==
                                                    2
                                                    ? Constants.kitGradients[8]
                                                    : snapshot
                                                    .data
                                                    .visas[
                                                index]
                                                    .status ==
                                                    3
                                                    ? Constants.kitGradients[15]
                                                    : snapshot
                                                    .data
                                                    .visas[
                                                index]
                                                    .status ==
                                                    10
                                                    ? Constants.kitGradients[14]
                                                    : snapshot.data.visas[index]
                                                    .status ==
                                                    11
                                                    ? Constants.kitGradients[14]
                                                    : Constants.kitGradients[8],

                                                fontWeight: FontWeight.w400,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          onTap: () {},
                        ),
                      );
                    },
                  )
                :Center(child: Container(
              child: Text(
                "No data found!",
                overflow: TextOverflow.visible,
                style: TextStyle(
                  fontFamily: 'ProximaNova',
                  fontSize: 18.0,
                  color: Constants.kitGradients[3],
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),): Center(
                    child: JumpingDotsProgressIndicator(
                      fontSize: 70.0,
                      numberOfDots: 5,
                      color: Constants.kitGradients[3],
                    ),
                  );
          }),
    );
  }
}
