import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Color(0xFF016BA6),
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            // Navigator.pushAndRemoveUntil(
            //     context,
            //     MaterialPageRoute(builder: (context) => HomeScreen()),
            //         (route) => false);
            Navigator.pop(context);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: screenWidth(context,dividedBy: 7),),
            Text(
              'Privacy Policy',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Privacy Policy',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Column(
                  children: [
                    Text(
                      'Smart Travel LLC built the Smart Travel app as a Free app. This SERVICE is provided by Smart Travel LLC at no cost and is intended for use as is.',
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      'This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.'
                     'If you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.',
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      'The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Smart Travel unless otherwise defined in this Privacy Policy.',
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    // Text(
                    //   'You should be aware that there are certain things that Smart Travel LLC will not take responsibility for. Certain functions of the app will require the app to have an active internet connection. The connection can be Wi-Fi, or provided by your mobile network provider, but Smart Travel LLC cannot take responsibility for the app not working at full functionality if you don’t have access to Wi-Fi, and you don’t have any of your data allowance left.',
                    //   overflow: TextOverflow.visible,
                    //   style: TextStyle(
                    //     fontFamily: 'ProximaNova',
                    //     fontSize: 12.0,
                    //     color: Constants.kitGradients[4],
                    //     fontWeight: FontWeight.w400,
                    //   ),
                    // ),
                    // Text(
                    //   'If you’re using the app outside of an area with Wi-Fi, you should remember that your terms of the agreement with your mobile network provider will still apply. As a result, you may be charged by your mobile provider for the cost of data for the duration of the connection while accessing the app, or other third party charges. In using the app, you’re accepting responsibility for any such charges, including roaming data charges if you use the app outside of your home territory (i.e. region or country) without turning off data roaming. If you are not the bill payer for the device on which you’re using the app, please be aware that we assume that you have received permission from the bill payer for using the app.',
                    //   overflow: TextOverflow.visible,
                    //   style: TextStyle(
                    //     fontFamily: 'ProximaNova',
                    //     fontSize: 12.0,
                    //     color: Constants.kitGradients[4],
                    //     fontWeight: FontWeight.w400,
                    //   ),
                    // ),
                    // Text(
                    //   'Along the same lines, Smart Travel LLC cannot always take responsibility for the way you use the app i.e. You need to make sure that your device stays charged – if it runs out of battery and you can’t turn it on to avail the Service, Smart Travel LLC cannot accept responsibility.',
                    //   overflow: TextOverflow.visible,
                    //   style: TextStyle(
                    //     fontFamily: 'ProximaNova',
                    //     fontSize: 12.0,
                    //     color: Constants.kitGradients[4],
                    //     fontWeight: FontWeight.w400,
                    //   ),
                    // ),
                    // Text(
                    //   'With respect to Smart Travel LLC’s responsibility for your use of the app, when you’re using the app, it’s important to bear in mind that although we endeavour to ensure that it is updated and correct at all times, we do rely on third parties to provide information to us so that we can make it available to you. Smart Travel LLC accepts no liability for any loss, direct or indirect, you experience as a result of relying wholly on this functionality of the app.',
                    //   overflow: TextOverflow.visible,
                    //   style: TextStyle(
                    //     fontFamily: 'ProximaNova',
                    //     fontSize: 12.0,
                    //     color: Constants.kitGradients[4],
                    //     fontWeight: FontWeight.w400,
                    //   ),
                    // ),
                    // Text(
                    //   'At some point, we may wish to update the app. The app is currently available on Android & iOS – the requirements for both systems(and for any additional systems we decide to extend the availability of the app to) may change, and you’ll need to download the updates if you want to keep using the app. Smart Travel LLC does not promise that it will always update the app so that it is relevant to you and/or works with the Android & iOS version that you have installed on your device.',
                    //   overflow: TextOverflow.visible,
                    //   style: TextStyle(
                    //     fontFamily: 'ProximaNova',
                    //     fontSize: 12.0,
                    //     color: Constants.kitGradients[4],
                    //     fontWeight: FontWeight.w400,
                    //   ),
                    // ),
                    // Text(
                    //   'However, you promise to always accept updates to the application when offered to you, We may also wish to stop providing the app, and may terminate use of it at any time without giving notice of termination to you. Unless we tell you otherwise, upon any termination, (a) the rights and licenses granted to you in these terms will end; (b) you must stop using the app, and (if needed) delete it from your device.',
                    //   overflow: TextOverflow.visible,
                    //   style: TextStyle(
                    //     fontFamily: 'ProximaNova',
                    //     fontSize: 12.0,
                    //     color: Constants.kitGradients[4],
                    //     fontWeight: FontWeight.w400,
                    //   ),
                    // ),
                  ],
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Information Collection and Use',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                  'We may update our Terms and Conditions from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Terms and Conditions on this page.'
                   'These terms and conditions are effective as of 2021-01-20',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Log Data',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                  'We want to inform you that whenever you use our Service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (“IP”) address, device name, operating system version, the configuration of the app when utilizing our Service, the time and date of your use of the Service, and other statistics.',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Cookies',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                  "Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory."
                "This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.",
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Service Providers',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory."
                          "This Service does not use these “cookies” explicitly. However, the app may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this Service.",
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      "We may employ third-party companies and individuals due to the following reasons:",
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 30),
                        ),
                      Container(
                      height: 6.0,
                      width: 6.0,
                      decoration: new BoxDecoration(
                        color: Constants.kitGradients[4],
                        shape: BoxShape.circle,
                      ),
                    ),
                        Text(
                          "  To facilitate our Service;",
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 12.0,
                            color: Constants.kitGradients[4],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 30),
                        ),
                        Container(
                          height: 6.0,
                          width: 6.0,
                          decoration: new BoxDecoration(
                            color: Constants.kitGradients[4],
                            shape: BoxShape.circle,
                          ),
                        ),
                        Text(
                          "  To provide the Service on our behalf;",
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 12.0,
                            color: Constants.kitGradients[4],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 30),
                        ),
                        Container(
                          height: 6.0,
                          width: 6.0,
                          decoration: new BoxDecoration(
                            color: Constants.kitGradients[4],
                            shape: BoxShape.circle,
                          ),
                        ),
                        Text(
                          "  To perform Service-related services; or",
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 12.0,
                            color: Constants.kitGradients[4],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 30),
                        ),
                        Container(
                          height: 6.0,
                          width: 6.0,
                          decoration: new BoxDecoration(
                            color: Constants.kitGradients[4],
                            shape: BoxShape.circle,
                          ),
                        ),
                        Text(
                          "   To assist us in analyzing how our Service is used.",
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 12.0,
                            color: Constants.kitGradients[4],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Text(
                      "We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.",
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                        fontFamily: 'ProximaNova',
                        fontSize: 12.0,
                        color: Constants.kitGradients[4],
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Security',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                  'We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Links to Other Sites',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                  'These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.',
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Changes to This Privacy Policy',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                  'We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.'
                 "This policy is effective as of 2021-01-20",
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 40),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Contact Us',
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                      fontFamily: 'ProximaNova',
                      fontSize: 16.0,
                      color: Constants.kitGradients[4],
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.0),
                child: Text(
                    "If you have any questions or suggestions about our Privacy Policy, Please contact us at Promotions.smarttravel@gmail.com.",
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                    fontFamily: 'ProximaNova',
                    fontSize: 12.0,
                    color: Constants.kitGradients[4],
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
