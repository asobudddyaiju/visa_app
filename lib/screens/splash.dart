import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_travel/hive/hive.dart';
import 'package:smart_travel/screens/home_screen.dart';
import 'package:smart_travel/screens/login_page_2.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:firebase_auth/firebase_auth.dart';



class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash>
    with SingleTickerProviderStateMixin {
  Timer _timerControl;
  AnimationController animationController;
  Animation animIconHeight;
  Animation animIconWidth;
  Animation animIconColor;
  FirebaseAuth _auth = FirebaseAuth.instance;

  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => AppHive().getToken()!=null?HomeScreen():LoginPage2(auth: _auth)),
              (route) => false);
    });
  }

  @override
  void didChangeDependencies() async{

    await FlutterStatusbarManager.setHidden(true, animation:StatusBarAnimation.NONE);
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    animIconColor = ColorTween(
        begin: Constants.kitGradients[0], end: Constants.kitGradients[3])
        .animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.5, 0.6)));
    animIconHeight = Tween<double>(
      end: 40.0,
      begin: 80.0,
    ).animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.5, 0.6)));
    animIconWidth = Tween<double>(end: 40.0, begin: 100.0).animate(
        CurvedAnimation(
            parent: animationController, curve: Interval(0.5, 0.6)));
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    startTimer();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    animationController.dispose();
    _timerControl.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar(
              // backgroundColor: Constants.kitGradients[0],
              elevation: 0.0,
            )),
        body: Builder(
            builder: (context) => SafeArea(
                top: true,
                left: true,
                bottom: true,
                child: Stack(children: [
                  Container(
                    height: screenHeight(context, dividedBy: 1),
                    width: screenWidth(context, dividedBy: 1),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        // SizedBox(
                        //   height: screenHeight(context, dividedBy: 3.5),
                        // ),
                        Container(
                          height: 250,
                          width: 250,
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 0
                            ),
                            child: Image.asset(
                              "assets/images/splash_icon2.png",
                              // fit: BoxFit.fill,
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                ]))));
  }
}
