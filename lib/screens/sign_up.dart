import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:smart_travel/Auth/auth_service.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class SignUpClass extends StatefulWidget {
  FirebaseAuth auth;

  SignUpClass({this.auth});

  @override
  _SignUpClassState createState() => _SignUpClassState();
}

class _SignUpClassState extends State<SignUpClass> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  TextEditingController emailIdText = TextEditingController();
  TextEditingController passwordText = TextEditingController();
  bool progressStatus = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 4.0),
            ),
            Text(
              'Sign up',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: progressStatus == true
          ? Center(
              child: JumpingDotsProgressIndicator(
                fontSize: 70.0,
                numberOfDots: 5,
                color: Constants.kitGradients[3],
              ),
            )
          : Stack(
              children: [
                Positioned(
                  top: screenHeight(context, dividedBy: 70),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: TextField(
                        decoration: InputDecoration(
                          labelText: 'Email address',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 17,
                              fontStyle: FontStyle.normal,
                              fontFamily: 'ProximaNova',
                              color: Constants.kitGradients[4]),
                          hintText: 'Enter E mail address',
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              fontFamily: 'ProximaNova',
                              color: Constants.kitGradients[6]),
                        ),
                        controller: emailIdText,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: screenHeight(context, dividedBy: 10),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1),
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          labelText: 'Password',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 17,
                              fontStyle: FontStyle.normal,
                              fontFamily: 'ProximaNova',
                              color: Constants.kitGradients[4]),
                          hintText: 'Enter your password',
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              fontStyle: FontStyle.normal,
                              fontFamily: 'ProximaNova',
                              color: Constants.kitGradients[6]),
                        ),
                        controller: passwordText,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  top: screenHeight(context, dividedBy: 4.0),
                  left: screenWidth(context, dividedBy: 4.2),
                  child: GestureDetector(
                      child: Container(
                        width: screenWidth(context, dividedBy: 2.0),
                        height: screenHeight(context, dividedBy: 15),
                        decoration: BoxDecoration(
                            color: Constants.kitGradients[3],
                            borderRadius: BorderRadius.circular(21)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Sign Up',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 18.0,
                                color: Constants.kitGradients[1],
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () async {
                        setState(() {
                          progressStatus = true;
                        });
                        if (passwordText.text.toString().isNotEmpty &&
                            passwordText.text.toString().isNotEmpty) {
                          print(emailIdText.text.toString());
                          print(passwordText.text.toString());

                          String status = await Auth(auth: widget.auth).signUp(
                              emailIdText.text.toString(),
                              passwordText.text.toString());
                          if (status.trim() == 'Success') {
                            setState(() {
                              progressStatus = false;
                            });
                            Navigator.pushAndRemoveUntil(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => HomeScreen()),
                                (route) => false);
                          } else {
                            _showSnackbar(status);
                          }
                        } else {
                          _showSnackbar(Constants.LOGIN_FIELD_EMPTY);
                        }
                      }),
                ),
                Positioned(
                  bottom: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                          width: screenWidth(context, dividedBy: 1),
                          child: Image.asset(
                            'assets/images/login_image.png',
                            fit: BoxFit.fitWidth,
                          )),
                    ],
                  ),
                ),
              ],
            ),
    );
  }

  void _showSnackbar(String snackdata) {
    final snack = SnackBar(
      content: Text(
        snackdata,
        style: TextStyle(
            fontFamily: "ProximaNova",
            fontStyle: FontStyle.normal,
            fontSize: 12.0),
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Constants.kitGradients[8],
    );
    _globalKey.currentState.showSnackBar(snack);
  }
}
