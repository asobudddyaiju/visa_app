import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:smart_travel/Auth/auth_service.dart';
import 'package:smart_travel/screens/sign_up.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class LoginClass extends StatefulWidget {
  FirebaseAuth auth;

  LoginClass({this.auth});

  @override
  _LoginClassState createState() => _LoginClassState();
}

class _LoginClassState extends State<LoginClass> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  TextEditingController emailIdText = TextEditingController();
  TextEditingController passwordText = TextEditingController();
  bool progressStatus = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar:
      AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 4.0),
            ),
            Text(
              'Log in',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: progressStatus == true
          ? Center(
              child: JumpingDotsProgressIndicator(
                fontSize: 70.0,
                numberOfDots: 5,
                color: Constants.kitGradients[3],
              ),
            )
          : Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Stack(
                children: [
                  Positioned(
                    top: screenHeight(context, dividedBy:2.3),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: TextField(
                          decoration: InputDecoration(
                            labelText: 'Email address',
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'ProximaNova',
                                color: Constants.kitGradients[4]),
                            hintText: 'Enter E mail address',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'ProximaNova',
                                color: Constants.kitGradients[6]),
                          ),
                          controller: emailIdText,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: screenHeight(context, dividedBy: 1.9),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: TextField(
                          obscureText: true,
                          decoration: InputDecoration(
                            labelText: 'Password',
                            labelStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'ProximaNova',
                                color: Constants.kitGradients[4]),
                            hintText: 'Enter your password',
                            hintStyle: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'ProximaNova',
                                color: Constants.kitGradients[6]),
                          ),
                          controller: passwordText,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: screenHeight(context, dividedBy: 1.5),
                    left: screenWidth(context, dividedBy: 4.2),
                    child: GestureDetector(
                        child: Container(
                          width: screenWidth(context, dividedBy: 2.0),
                          height: screenHeight(context, dividedBy: 15),
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[3],
                              borderRadius: BorderRadius.circular(21)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Sign In',
                                style: TextStyle(
                                  fontFamily: 'ProximaNova',
                                  fontSize: 18.0,
                                  color: Constants.kitGradients[1],
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                            ],
                          ),
                        ),
                        onTap: () async {
                          setState(() {
                            progressStatus = true;
                          });
                          if (passwordText.text.toString().isNotEmpty &&
                              passwordText.text.toString().isNotEmpty) {
                            print(emailIdText.text.toString());
                            print(passwordText.text.toString());

                            String status = await Auth(auth: widget.auth).signIn(
                                emailIdText.text.toString(),
                                passwordText.text.toString());
                            if (status.trim() == 'Success') {
                              setState(() {
                                progressStatus = false;
                              });
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomeScreen()),
                                  (route) => false);
                            } else {
                              _showSnackbar(status);
                            }
                          } else {
                            _showSnackbar(Constants.LOGIN_FIELD_EMPTY);
                          }
                        }),
                  ),
                  Positioned(
                    top: screenHeight(context, dividedBy: 1.35),
                    left: screenWidth(context, dividedBy: 4.2),
                    child: GestureDetector(
                      child: Container(
                        width: screenWidth(context, dividedBy: 2.0),
                        height: screenHeight(context, dividedBy: 15),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(21),
                            border: Border.all(color: Constants.kitGradients[3])),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Sign in with google',
                              style: TextStyle(
                                fontFamily: 'ProximaNova',
                                fontSize: 18.0,
                                color: Constants.kitGradients[3],
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                          ],
                        ),
                      ),
                      onTap: () async {
                        setState(() {
                          progressStatus = true;
                        });
                        String status =
                            await Auth(auth: widget.auth).signInWithGoogle();
                        if (status.trim() == 'Success') {
                          setState(() {
                            progressStatus = false;
                          });
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeScreen()),
                              (route) => false);
                        } else {
                          _showSnackbar(status);
                        }
                      },
                    ),
                  ),
                  Positioned(
                    top: screenHeight(context, dividedBy: 1.2),
                    left: screenWidth(context, dividedBy: 4.5),
                    child: SafeArea(
                      child: Center(
                        child: Padding(
                          padding:
                              EdgeInsets.only(bottom: 2, left: 15.0, right: 15.0),
                          child: RichText(
                            text: TextSpan(
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => SignUpClass(
                                                  auth: widget.auth,
                                                )));
                                  },
                                text: 'Don’t have an account?',
                                style: TextStyle(
                                  fontFamily: 'NexaLight',
                                  fontWeight: FontWeight.w400,
                                  color: Color(0xFF404040),
                                  fontSize: 12.0,
                                ),
                                children: <TextSpan>[
                                  TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () async {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SignUpClass(
                                                        auth: widget.auth,
                                                      )));
                                        },
                                      text: 'Sign up',
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontWeight: FontWeight.w400,
                                          color: Colors.blue,
                                          fontSize: 12.0)),
                                ]),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        child: Image.asset(
                          'assets/images/login_page_icon.png',
                          fit: BoxFit.fitWidth,
                        )),
                  ),
                ],
              ),
          ),
    );
  }

  void _showSnackbar(String snackdata) {
    final snack = SnackBar(
      content: Text(
        snackdata,
        style: TextStyle(
            fontFamily: "Poppins", fontStyle: FontStyle.normal, fontSize: 12.0),
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Constants.kitGradients[8],
    );
    _globalKey.currentState.showSnackBar(snack);
  }
}
