import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smart_travel/Auth/auth_service.dart';
import 'package:smart_travel/bloc/user_bloc.dart';
import 'package:smart_travel/hive/hive.dart';
import 'package:smart_travel/screens/privacy_policy.dart';
import 'package:smart_travel/screens/terms_and_conditions.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';
import 'package:progress_indicators/progress_indicators.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'home_screen.dart';

class LoginPage2 extends StatefulWidget {
  FirebaseAuth auth;

  LoginPage2({this.auth});

  @override
  _LoginPage2State createState() => _LoginPage2State();
}

class _LoginPage2State extends State<LoginPage2> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  bool progressStatus = false;
  AppBloc appBloc = AppBloc();
@override
  void initState() {
  appBloc.loginResponse.listen((event) {
    print("token :"+event.drfToken);
    AppHive().putToken(token: event.drfToken);
    // setState(() {
    //   progressStatus = false;
    // });
  });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 4.0),
            ),
            Text(
              'Log in',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: progressStatus==true?
      Center(
        child: JumpingDotsProgressIndicator(
          fontSize: 70.0,
          numberOfDots: 5,
          color: Constants.kitGradients[3],
        ),
      )
      :Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [

          SizedBox(
            height: screenWidth(context, dividedBy: 15),
          ),
          Center(
            child: Container(
              width: screenWidth(context, dividedBy: 1.1),
              height: screenHeight(context, dividedBy: 2.1),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/login_page_icon.png'),
                    fit: BoxFit.fill),
              ),
            ),
          ),
          Center(
            child: GestureDetector(
              onTap: () async {
                setState(() {
                  progressStatus = true;
                });
                checkDataConnection();
                String status =
                    await Auth(auth: widget.auth).signInWithGoogle();
                print("start"+Auth(auth: widget.auth).signInWithGoogle().toString());
                if (status.trim() == 'Success') {

                  await appBloc.login(uuid: AppHive().hiveGet(key: "uuid"));
                  setState(() {
                    progressStatus = false;
                  });
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => HomeScreen()),
                      (route) => false);
                } else {
                  _showSnackbar(status);
                }
              },
              child: Container(
                width: screenWidth(context, dividedBy: 1.3),
                height: screenHeight(context, dividedBy: 15),
                decoration: BoxDecoration(
                    border: Border.all(style: BorderStyle.none),
                    color: Constants.kitGradients[1],
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: [
                      BoxShadow(
                          color: Constants.kitGradients[2].withOpacity(0.2),
                          offset: Offset(0, 2),
                          blurRadius: 5.0,
                          spreadRadius: 4.0)
                    ]),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/images/google_icon.svg',
                          width: 20.0, height: 20.0),
                      Padding(
                        padding: EdgeInsets.only(left: 10.0),
                        child: Text(
                          'Continue with Google',
                          style: TextStyle(
                              fontFamily: 'NexaLight',
                              fontSize: 16.0,
                              fontWeight: FontWeight.w300,
                              fontStyle: FontStyle.normal,
                              color: Color(0xFF484848)),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: screenWidth(context, dividedBy: 3.3),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TermsConditions()));
            },
            child: Container(
              height: screenHeight(context, dividedBy: 15),
              width: screenWidth(context, dividedBy: 1),
              child: Center(
                child: RichText(
                  text: TextSpan(
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TermsConditions()));
                        },
                      text: 'By signing up you agree to our',
                      style: TextStyle(
                        fontFamily: 'NexaLight',
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF404040),
                        fontSize: 10.0,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () async {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            TermsConditions()));
                              },
                            text: ' Terms & Condition ',
                            style: TextStyle(
                                fontFamily: 'NexaLight',
                                fontWeight: FontWeight.w400,
                                color: Colors.blue,
                                fontSize: 10.0)),
                        TextSpan(
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => TermsConditions()));
                            },
                          text: ' and ',
                          style: TextStyle(
                            fontFamily: 'NexaLight',
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF404040),
                            fontSize: 10.0,
                          ),
                        ),
                        TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () async {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            PrivacyPolicy()));
                              },
                            text: 'Privacy Policy',
                            style: TextStyle(
                                fontFamily: 'NexaLight',
                                fontWeight: FontWeight.w400,
                                color: Colors.blue,
                                fontSize: 10.0)),
                      ]),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _showSnackbar(String snackdata) {
    final snack = SnackBar(
      content: Text(
        snackdata,
        style: TextStyle(
            fontFamily: "Poppins", fontStyle: FontStyle.normal, fontSize: 12.0),
      ),
      duration: Duration(seconds: 3),
      backgroundColor: Constants.kitGradients[8],
    );
    _globalKey.currentState.showSnackBar(snack);
  }
  checkDataConnection() async {
    bool status = await DataConnectionChecker().hasConnection;
    if (status == true) {
      print('internet connection ok');
    } else {
      final snack = SnackBar(
        content: Text('No internet connection'),
        duration: Duration(seconds: 3),
      );
      setState(() {
        progressStatus = false;
      });
      _globalKey.currentState.showSnackBar(snack);
    }
  }
}
