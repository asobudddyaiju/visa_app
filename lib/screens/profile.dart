import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:smart_travel/hive/hive.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class ProfilePage extends StatefulWidget {
  FirebaseAuth auth;

  ProfilePage({this.auth});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String photpurl;
  String displayName;
  String dummydisplayName = 'username';
  String email = 'username';
  String dummyemail = 'username@gmail.com';
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    if (widget.auth.currentUser != null) {
    } else {
      setState(() {
        displayName = dummydisplayName;
        email = dummyemail;
      });
    }
    if ((AppHive().hiveGet(key: 'name') != null) ||
        (AppHive().hiveGet(key: 'photourl') != null)) {
      setState(() {
        photpurl = AppHive().hiveGet(key: 'photourl');
        displayName = AppHive().hiveGet(key: 'name');
        email = AppHive().hiveGet(key: 'email');
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 4.0),
            ),
            Text(
              'Profile',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 25),
          ),
          Center(
            child: Container(
              width: screenWidth(context, dividedBy: 2),
              height: screenHeight(context, dividedBy: 5.0),
              decoration: BoxDecoration(
                  color: Constants.kitGradients[1],
                  shape: BoxShape.circle,
                  border:
                      Border.all(color: Constants.kitGradients[9], width: 2)),
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.black.withOpacity(.05),
                  shape: BoxShape.circle,
                  border:
                      Border.all(color: Constants.kitGradients[1], width: 2),
                  image: DecorationImage(
                      image: widget.auth.currentUser != null?NetworkImage(photpurl):AssetImage('assets/images/account.png'), fit: BoxFit.fill),
                ),
              ),
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          Text(
            displayName,
            style: TextStyle(
              fontFamily: 'ProximaNova',
              fontSize: 26.0,
              color: Constants.kitGradients[3],
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          Text(
            email,
            style: TextStyle(
              fontFamily: 'ProximaNova',
              fontSize: 14.0,
              color: Constants.kitGradients[10],
              fontWeight: FontWeight.w400,
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          // Text(
          //  ' +971 4 273 5857',
          //   style: TextStyle(
          //     fontFamily: 'ProximaNova',
          //     fontSize: 14.0,
          //     color: Constants.kitGradients[10],
          //     fontWeight: FontWeight.w400,
          //   ),
          // ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
         Row(
           mainAxisAlignment: MainAxisAlignment.center,
           children: [
             SvgPicture.asset('assets/images/edit.svg'),
             SizedBox(
               width: screenWidth(context,dividedBy: 200),
             ),
             Text(
               'Edit',
               style: TextStyle(
                 fontFamily: 'ProximaNova',
                 fontSize: 14.0,
                 color: Constants.kitGradients[3],
                 fontWeight: FontWeight.w400,
               ),
             ),
           ],
         )
        ],
      ),
    );
  }
}
