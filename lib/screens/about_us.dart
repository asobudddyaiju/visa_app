import 'package:flutter/material.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/util.dart';

import 'home_screen.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUsState createState() => _AboutUsState();
}

class _AboutUsState extends State<AboutUs> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Constants.kitGradients[1],
      appBar: AppBar(
        leading: GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 10),
            color: Constants.kitGradients[3],
            child: Icon(
              Icons.arrow_back_ios,
              color: Constants.kitGradients[1],
              size: 18,
            ),
          ),
          onTap: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomeScreen()),
                (route) => false);
          },
        ),
        toolbarHeight: screenHeight(context, dividedBy: 15),
        backgroundColor: Constants.kitGradients[3],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(width: screenWidth(context,dividedBy: 4.8),),
            Text(
              'About us',
              style: TextStyle(
                fontFamily: 'ProximaNova',
                fontSize: 16.0,
                color: Constants.kitGradients[1],
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
      body: Stack(
        children: [
          Positioned(
              top: 0,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: screenHeight(context, dividedBy: 45),
                    horizontal: screenWidth(context, dividedBy: 25)),
                child: Column(
                  children: [
                    Container(
                      width: screenWidth(context, dividedBy: 1.0),
                      child: Padding(
                        padding: EdgeInsets.only(
                            right: screenWidth(context, dividedBy: 20)),
                        child: Text(
                          '    Smart Travel LLC is a full service travel agency dedicated to providing Corporate, leisure and retail travel services. Smart Travel LLC is just not a travel agency; we help you weave your travel dreams. Smart Travel as an entity takes care of any tour and travel requirement of its esteemed clients.',
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            fontFamily: 'ProximaNova',
                            fontSize: 12.0,
                            color: Constants.kitGradients[4],
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 80),
                    ),
                    Container(
                      width: screenWidth(context, dividedBy: 1.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              RichText(
                                text: TextSpan(
                                    text: 'CONTACT ',
                                    style: TextStyle(
                                      fontFamily: 'ProximaNova',
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.w700,
                                      color: Constants.kitGradients[4],
                                    ),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: 'US',
                                        style: TextStyle(
                                          fontFamily: 'ProximaNova',
                                          fontSize: 20.0,
                                          color: Constants.kitGradients[4],
                                          fontWeight: FontWeight.w400,
                                        ),
                                      )
                                    ]),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Container(
                                width: screenWidth(context, dividedBy: 7.5),
                                height: screenHeight(context, dividedBy: 160),
                                decoration: BoxDecoration(
                                    color: Constants.kitGradients[3],
                                    borderRadius: BorderRadius.circular(18)),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical:
                                        screenHeight(context, dividedBy: 38)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'SMART TRAVELS',
                                      overflow: TextOverflow.visible,
                                      style: TextStyle(
                                        fontFamily: 'ProximaNova',
                                        fontSize: 12.0,
                                        color: Constants.kitGradients[4],
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 80),
                                    ),
                                    Text(
                                      'Deira - Dubai - United Arab Emirates',
                                      overflow: TextOverflow.visible,
                                      style: TextStyle(
                                        fontFamily: 'ProximaNova',
                                        fontSize: 12.0,
                                        color: Constants.kitGradients[4],
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 80),
                                    ),
                                    Text(
                                      'Phone: +971 4 273 7777',
                                      overflow: TextOverflow.visible,
                                      style: TextStyle(
                                        fontFamily: 'ProximaNova',
                                        fontSize: 12.0,
                                        color: Constants.kitGradients[4],
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )),
          Positioned(
            bottom: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Image.asset(
                      'assets/images/about_us_icon.png',
                      fit: BoxFit.fitWidth,
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
