import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/models/contact_us_request.dart';
import 'package:smart_travel/models/state.dart';
import 'package:smart_travel/models/status_request.dart';
import 'package:smart_travel/models/upload_image_request.dart';
import 'package:smart_travel/resources/api_providers/api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final apiProvider = ApiProvider();

  Future<State> visaList() => apiProvider.visaList();

  Future<State> applyVisa({ApplyVisaRequest applyVisaRequest}) =>
      apiProvider.applyVisa(applyVisaRequest);

  Future<State> uploadImage({UploadImageRequest uploadImageRequest}) =>
      apiProvider.uploadImage(uploadImageRequest);

  Future<State> contactUs({ContactUsRequest contactUsRequest}) =>
      apiProvider.contactUs(contactUsRequest);

  Future<State> statusCheck({StatusRequest statusRequest}) =>
      apiProvider.statusCheck(statusRequest);

  Future<State> login({String uuid}) => apiProvider.login(uuid);

  Future<State> myVisa() => apiProvider.myVisa();

  Future<State> getPaymentUrl(String name, String mobile, String amount) =>
      apiProvider.getPaymentUrl(name, mobile, amount);
}
