import 'dart:convert';

import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/models/apply_visa_response.dart';
import 'package:smart_travel/models/contact_us_request.dart';
import 'package:smart_travel/models/contact_us_response.dart';
import 'package:smart_travel/models/login_response_model.dart';
import 'package:smart_travel/models/my_vis_response.dart';
import 'package:smart_travel/models/payment_response_model.dart';
import 'package:smart_travel/models/state.dart';
import 'package:smart_travel/models/status_request.dart';
import 'package:smart_travel/models/status_response.dart';
import 'package:smart_travel/models/upload_image_request.dart';
import 'package:smart_travel/models/upload_image_response.dart';
import 'package:smart_travel/models/visa_list_model.dart';
import 'package:smart_travel/utils/object_factory.dart';
import 'package:xml2json/xml2json.dart';

class ApiProvider {
  final myTransformer = Xml2Json();

  Future<State> visaList() async {
    final response = await ObjectFactory().apiClient.visaList();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<VisaListResponse>.success(
          VisaListResponse.fromJson(response.data));
    } else {
      return State<VisaListResponse>.error(
          VisaListResponse.fromJson(response.data));
    }
  }

  Future<State> applyVisa(ApplyVisaRequest applyVisaRequest) async {
    final response =
        await ObjectFactory().apiClient.applyVisa(applyVisaRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<ApplyVisaResponse>.success(
          ApplyVisaResponse.fromJson(response.data));
    } else {
      return State<ApplyVisaResponse>.error(
          ApplyVisaResponse.fromJson(response.data));
    }
  }

  Future<State> uploadImage(UploadImageRequest uploadImageRequest) async {
    final response =
        await ObjectFactory().apiClient.uploadImage(uploadImageRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<UploadImageResponse>.success(
          UploadImageResponse.fromJson(response.data));
    } else {
      return State<UploadImageResponse>.error(
          UploadImageResponse.fromJson(response.data));
    }
  }

  Future<State> contactUs(ContactUsRequest contactUsRequest) async {
    final response =
        await ObjectFactory().apiClient.contactUs(contactUsRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<ContactUsResponse>.success(
          ContactUsResponse.fromJson(response.data));
    } else {
      return State<ContactUsResponse>.error(
          ContactUsResponse.fromJson(response.data));
    }
  }

  Future<State> statusCheck(StatusRequest statusRequest) async {
    final response = await ObjectFactory().apiClient.statusCheck(statusRequest);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<StatusResponse>.success(
          StatusResponse.fromJson(response.data));
    } else {
      return State<StatusResponse>.error(
          StatusResponse.fromJson(response.data));
    }
  }

  Future<State> myVisa() async {
    final response = await ObjectFactory().apiClient.myVisa();
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<MyVisaResponse>.success(
          MyVisaResponse.fromJson(response.data));
    } else {
      return State<MyVisaResponse>.error(
          MyVisaResponse.fromJson(response.data));
    }
  }

  Future<State> login(String uuid) async {
    final response = await ObjectFactory().apiClient.login(uuid);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      return State<LoginResponseModel>.success(
          LoginResponseModel.fromJson(response.data));
    } else {
      return State<LoginResponseModel>.error(
          LoginResponseModel.fromJson(response.data));
    }
  }

  Future<State> getPaymentUrl(String name, String mobile, String amount) async {
    final response =
        await ObjectFactory().apiClient.getPaymentUrl(name, mobile, amount);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 201) {
      myTransformer.parse(response.data);
      print(myTransformer.toGData());
      return State<PaymentResponseModel>.success(
          PaymentResponseModel.fromJson(jsonDecode(myTransformer.toGData())));
    } else {
      return State<PaymentResponseModel>.error(
          PaymentResponseModel.fromJson(jsonDecode(myTransformer.toGData())));
    }
  }
}
