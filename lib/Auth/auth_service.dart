import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:smart_travel/hive/hive.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/object_factory.dart';

class Auth {
  FirebaseAuth auth;
  Auth({this.auth});
  Future<String> signUp(String email, String password) async {
    try {
      UserCredential userCredential = await auth.createUserWithEmailAndPassword(
          email: email.trim(), password: password.trim());
      if (userCredential.user != null) {
        AppHive().hivePut(key: 'name', value: email.split('@').first);
        return "Success";
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        return "The password provided is too weak.";
      } else if (e.code == 'email-already-in-use') {
        print('An account already exists for that email.');
        return "An account already exists for that email.";
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<String> signIn(String email, String password) async {
    try {
      UserCredential userCredential = await auth.signInWithEmailAndPassword(
          email: email.trim(), password: password.trim());
      if (userCredential.user != null) {
        AppHive().hivePut(key: 'name', value: email.split('@').first);
        return "Success";
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
        return "No user found for that email.";
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
        return "Wrong password provided for that user.";
      }
    }
  }

  Future<String> signInWithGoogle() async {
    try {
      // Trigger the authentication flow
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      // Create a new credential
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await auth.signInWithCredential(credential);
      if (googleUser != null) {
        print("idToken is "+googleAuth.idToken);
        print("accessToken is "+googleAuth.accessToken);
        AppHive()
            .hivePut(key: 'name', value: googleUser.displayName.toString());
        AppHive()
            .hivePut(key: 'photourl', value: googleUser.photoUrl.toString());
        AppHive().hivePut(key: 'id', value: googleUser.id.toString());
        AppHive().hivePut(key: 'email', value: googleUser.email.toString());
        AppHive().hivePut(key: 'uuid', value: auth.currentUser.uid.toString());


        ObjectFactory().appHive.putUserName(name: googleUser.displayName.toString().trim());
        ObjectFactory().appHive.putUId(userId: auth.currentUser.uid.toString().trim());
        ObjectFactory().appHive.putUserEmail(email: googleUser.email.toString().trim());
      }
      String status = "Success";
      return status.trim();
    } on FirebaseAuthException catch (e) {
      return e.message;
    } catch (e) {
      rethrow;
    }
  }

  Future<String> signOut() async {
    try {
      await auth.signOut();
      await GoogleSignIn().signOut();
      String status = "Success";
      return status.trim();
    } on FirebaseAuthException catch (e) {
      return e.message;
    } catch (e) {
      rethrow;
    }
  }
}
