import 'package:hive/hive.dart';
import 'package:smart_travel/utils/constants.dart';

class AppHive {
  static const String _UID = "uid";
  static const String _USER_NAME = "user_name";
  static const String _PHOTO_URL = "photo";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  putUId({String userId}) {
    hivePut(key: _UID, value: userId);
  }

  String getUId() {
    return hiveGet(key: _UID);
  }

  putUserName({String name}) {
    hivePut(key: _USER_NAME, value: name);
  }

  String getUserName() {
    return hiveGet(key: _USER_NAME);
  }

  putUserEmail({String email}) {
    hivePut(key: _EMAIL, value: email);
  }

  String getUserEmail() {
    return hiveGet(key: _EMAIL);
  }

  putPhoto({String photo}) {
    hivePut(key: _PHOTO_URL, value: photo);
  }

  String getPhoto() {
    return hiveGet(key: _PHOTO_URL);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  AppHive();
}
