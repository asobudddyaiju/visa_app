// To parse this JSON data, do
//
//     final statusRequest = statusRequestFromJson(jsonString);

import 'dart:convert';

StatusRequest statusRequestFromJson(String str) =>
    StatusRequest.fromJson(json.decode(str));

String statusRequestToJson(StatusRequest data) => json.encode(data.toJson());

class StatusRequest {
  StatusRequest({
    this.apiKey,
    this.applicationId,
  });

  final String apiKey;
  final String applicationId;

  factory StatusRequest.fromJson(Map<String, dynamic> json) => StatusRequest(
        apiKey: json["api_key"] == null ? null : json["api_key"],
        applicationId:
            json["Application_id"] == null ? null : json["Application_id"],
      );

  Map<String, dynamic> toJson() => {
        "api_key": apiKey == null ? null : apiKey,
        "Application_id": applicationId == null ? null : applicationId,
      };
}
