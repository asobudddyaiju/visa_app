// To parse this JSON data, do
//
//     final paymentResponseModel = paymentResponseModelFromJson(jsonString);

import 'dart:convert';

PaymentResponseModel paymentResponseModelFromJson(String str) =>
    PaymentResponseModel.fromJson(json.decode(str));

String paymentResponseModelToJson(PaymentResponseModel data) =>
    json.encode(data.toJson());

class PaymentResponseModel {
  PaymentResponseModel({
    this.root,
  });

  final Root root;

  factory PaymentResponseModel.fromJson(Map<String, dynamic> json) =>
      PaymentResponseModel(
        root: json["Root"] == null ? null : Root.fromJson(json["Root"]),
      );

  Map<String, dynamic> toJson() => {
        "Root": root == null ? null : root.toJson(),
      };
}

class Root {
  Root({
    this.status,
    this.statusCode,
    this.message,
    this.operation,
    this.resultFormat,
    this.transactionStatus,
    this.result,
  });

  final Message status;
  final Message statusCode;
  final Message message;
  final Message operation;
  final Message resultFormat;
  final TransactionStatus transactionStatus;
  final Result result;

  factory Root.fromJson(Map<String, dynamic> json) => Root(
        status:
            json["Status"] == null ? null : Message.fromJson(json["Status"]),
        statusCode: json["StatusCode"] == null
            ? null
            : Message.fromJson(json["StatusCode"]),
        message:
            json["Message"] == null ? null : Message.fromJson(json["Message"]),
        operation: json["Operation"] == null
            ? null
            : Message.fromJson(json["Operation"]),
        resultFormat: json["ResultFormat"] == null
            ? null
            : Message.fromJson(json["ResultFormat"]),
        transactionStatus: json["TransactionStatus"] == null
            ? null
            : TransactionStatus.fromJson(json["TransactionStatus"]),
        result: json["Result"] == null ? null : Result.fromJson(json["Result"]),
      );

  Map<String, dynamic> toJson() => {
        "Status": status == null ? null : status.toJson(),
        "StatusCode": statusCode == null ? null : statusCode.toJson(),
        "Message": message == null ? null : message.toJson(),
        "Operation": operation == null ? null : operation.toJson(),
        "ResultFormat": resultFormat == null ? null : resultFormat.toJson(),
        "TransactionStatus":
            transactionStatus == null ? null : transactionStatus.toJson(),
        "Result": result == null ? null : result.toJson(),
      };
}

class Message {
  Message({
    this.t,
  });

  final String t;

  factory Message.fromJson(Map<String, dynamic> json) => Message(
        t: json["\u0024t"] == null ? null : json["\u0024t"],
      );

  Map<String, dynamic> toJson() => {
        "\u0024t": t == null ? null : t,
      };
}

class Result {
  Result({
    this.cdata,
  });

  final String cdata;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        cdata: json["__cdata"] == null ? null : json["__cdata"],
      );

  Map<String, dynamic> toJson() => {
        "__cdata": cdata == null ? null : cdata,
      };
}

class TransactionStatus {
  TransactionStatus();

  factory TransactionStatus.fromJson(Map<String, dynamic> json) =>
      TransactionStatus();

  Map<String, dynamic> toJson() => {};
}
