// To parse this JSON data, do
//
//     final contactUsRequest = contactUsRequestFromJson(jsonString);

import 'dart:convert';

ContactUsRequest contactUsRequestFromJson(String str) => ContactUsRequest.fromJson(json.decode(str));

String contactUsRequestToJson(ContactUsRequest data) => json.encode(data.toJson());

class ContactUsRequest {
  ContactUsRequest({
    this.subject,
    this.message,
  });

  final String subject;
  final String message;

  factory ContactUsRequest.fromJson(Map<String, dynamic> json) => ContactUsRequest(
    subject: json["subject"] == null ? null : json["subject"],
    message: json["message"] == null ? null : json["message"],
  );

  Map<String, dynamic> toJson() => {
    "subject": subject == null ? null : subject,
    "message": message == null ? null : message,
  };
}
