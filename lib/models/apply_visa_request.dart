// To parse this JSON data, do
//
//     final applyVisaRequest = applyVisaRequestFromJson(jsonString);

import 'dart:convert';

ApplyVisaRequest applyVisaRequestFromJson(String str) =>
    ApplyVisaRequest.fromJson(json.decode(str));

String applyVisaRequestToJson(ApplyVisaRequest data) =>
    json.encode(data.toJson());

class ApplyVisaRequest {
  ApplyVisaRequest({
    this.apiKey,
    this.visa,
    this.name,
    this.email,
    this.countryCode,
    this.contactNumber,
    this.dateOfEntry,
    this.nationality,
    this.passportPage1,
    this.passportPage2,
    this.passportPage3,
    this.personalPhoto,
    this.ticketCopy,
  });

  final String apiKey;
  final String visa;
  final String name;
  final String email;
  final String countryCode;
  final String contactNumber;
  final String dateOfEntry;
  final String nationality;
  final String passportPage1;
  final String passportPage2;
  final String passportPage3;
  final String personalPhoto;
  final String ticketCopy;

  factory ApplyVisaRequest.fromJson(Map<String, dynamic> json) =>
      ApplyVisaRequest(
        apiKey: json["api_key"] == null ? null : json["api_key"],
        visa: json["visa_id"] == null ? null : json["visa"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        contactNumber:
            json["contact_number"] == null ? null : json["contact_number"],
        dateOfEntry:
            json["date_of_entry"] == null ? null : json["date_of_entry"],
        nationality: json["nationality"] == null ? null : json["nationality"],
        passportPage1:
            json["passport_page_1"] == null ? null : json["passport_page_1"],
        passportPage2:
            json["passport_page_2"] == null ? null : json["passport_page_2"],
        passportPage3:
            json["passport_page_3"] == null ? null : json["passport_page_3"],
        personalPhoto:
            json["personal_photo"] == null ? null : json["personal_photo"],
        ticketCopy: json["ticket_copy"] == null ? null : json["ticket_copy"],
      );

  Map<String, dynamic> toJson() => {
        "api_key": apiKey == null ? null : apiKey,
        "visa_id": visa == null ? null : visa,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "country_code": countryCode == null ? null : countryCode,
        "contact_number": contactNumber == null ? null : contactNumber,
        "date_of_entry": dateOfEntry == null ? null : dateOfEntry,
        "nationality": nationality == null ? null : nationality,
        "passport_page_1": passportPage1 == null ? null : passportPage1,
        "passport_page_2": passportPage2 == null ? null : passportPage2,
        "passport_page_3": passportPage3 == null ? null : passportPage3,
        "personal_photo": personalPhoto == null ? null : personalPhoto,
        "ticket_copy": ticketCopy == null ? null : ticketCopy,
      };
}
