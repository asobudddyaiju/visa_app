// To parse this JSON data, do
//
//     final uploadImageRequest = uploadImageRequestFromJson(jsonString);

import 'dart:convert';
import 'dart:io';

UploadImageRequest uploadImageRequestFromJson(String str) =>
    UploadImageRequest.fromJson(json.decode(str));

String uploadImageRequestToJson(UploadImageRequest data) =>
    json.encode(data.toJson());

class UploadImageRequest {
  UploadImageRequest({
    // this.apiKey,
    this.imageFile,
  });

  // final String apiKey;
  final File imageFile;

  factory UploadImageRequest.fromJson(Map<String, dynamic> json) =>
      UploadImageRequest(
        // apiKey: json["api_key"] == null ? null : json["api_key"],
        imageFile: json["Image_file"] == null ? null : json["Image_file"],
      );

  Map<String, dynamic> toJson() => {
        // "api_key": apiKey == null ? null : apiKey,
        "Image_file": imageFile == null ? null : imageFile,
      };
}
