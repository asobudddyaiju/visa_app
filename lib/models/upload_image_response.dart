// To parse this JSON data, do
//
//     final uploadImageResponse = uploadImageResponseFromJson(jsonString);

import 'dart:convert';

UploadImageResponse uploadImageResponseFromJson(String str) => UploadImageResponse.fromJson(json.decode(str));

String uploadImageResponseToJson(UploadImageResponse data) => json.encode(data.toJson());

class UploadImageResponse {
  UploadImageResponse({
    this.status,
    this.message,
    this.file,
  });

  final int status;
  final String message;
  final FileClass file;

  factory UploadImageResponse.fromJson(Map<String, dynamic> json) => UploadImageResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    file: json["file"] == null ? null : FileClass.fromJson(json["file"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "file": file == null ? null : file.toJson(),
  };
}

class FileClass {
  FileClass({
    this.id,
    this.imageFile,
    this.created,
    this.user,
  });

  final int id;
  final String imageFile;
  final DateTime created;
  final int user;

  factory FileClass.fromJson(Map<String, dynamic> json) => FileClass(
    id: json["id"] == null ? null : json["id"],
    imageFile: json["image_file"] == null ? null : json["image_file"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "image_file": imageFile == null ? null : imageFile,
    "created": created == null ? null : created.toIso8601String(),
    "user": user == null ? null : user,
  };
}
