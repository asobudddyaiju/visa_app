// To parse this JSON data, do
//
//     final myVisaResponse = myVisaResponseFromJson(jsonString);

import 'dart:convert';

MyVisaResponse myVisaResponseFromJson(String str) => MyVisaResponse.fromJson(json.decode(str));

String myVisaResponseToJson(MyVisaResponse data) => json.encode(data.toJson());

class MyVisaResponse {
  MyVisaResponse({
    this.status,
    this.message,
    this.visas,
  });

  final int status;
  final String message;
  final List<VisaElement> visas;

  factory MyVisaResponse.fromJson(Map<String, dynamic> json) => MyVisaResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    visas: json["visas"] == null ? null : List<VisaElement>.from(json["visas"].map((x) => VisaElement.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "visas": visas == null ? null : List<dynamic>.from(visas.map((x) => x.toJson())),
  };
}

class VisaElement {
  VisaElement({
    this.id,
    this.visa,
    this.applicationId,
    this.name,
    this.email,
    this.countryCode,
    this.contactNumber,
    this.dateOfEntry,
    this.nationality,
    this.passportPage1,
    this.passportPage2,
    this.passportPage3,
    this.personalPhoto,
    this.ticketCopy,
    this.status,
    this.created,
    this.user,
  });

  final int id;
  final VisaVisa visa;
  final String applicationId;
  final String name;
  final String email;
  final String countryCode;
  final String contactNumber;
  final DateTime dateOfEntry;
  final String nationality;
  final String passportPage1;
  final String passportPage2;
  final String passportPage3;
  final String personalPhoto;
  final String ticketCopy;
  final int status;
  final DateTime created;
  final int user;

  factory VisaElement.fromJson(Map<String, dynamic> json) => VisaElement(
    id: json["id"] == null ? null : json["id"],
    visa: json["visa"] == null ? null : VisaVisa.fromJson(json["visa"]),
    applicationId: json["application_id"] == null ? null : json["application_id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"],
    countryCode: json["country_code"] == null ? null : json["country_code"],
    contactNumber: json["contact_number"] == null ? null : json["contact_number"],
    dateOfEntry: json["date_of_entry"] == null ? null : DateTime.parse(json["date_of_entry"]),
    nationality: json["nationality"] == null ? null : json["nationality"],
    passportPage1: json["passport_page_1"] == null ? null : json["passport_page_1"],
    passportPage2: json["passport_page_2"] == null ? null : json["passport_page_2"],
    passportPage3: json["passport_page_3"] == null ? null : json["passport_page_3"],
    personalPhoto: json["personal_photo"] == null ? null : json["personal_photo"],
    ticketCopy: json["ticket_copy"] == null ? null : json["ticket_copy"],
    status: json["status"] == null ? null : json["status"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "visa": visa == null ? null : visa.toJson(),
    "application_id": applicationId == null ? null : applicationId,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
    "country_code": countryCode == null ? null : countryCode,
    "contact_number": contactNumber == null ? null : contactNumber,
    "date_of_entry": dateOfEntry == null ? null : "${dateOfEntry.year.toString().padLeft(4, '0')}-${dateOfEntry.month.toString().padLeft(2, '0')}-${dateOfEntry.day.toString().padLeft(2, '0')}",
    "nationality": nationality == null ? null : nationality,
    "passport_page_1": passportPage1 == null ? null : passportPage1,
    "passport_page_2": passportPage2 == null ? null : passportPage2,
    "passport_page_3": passportPage3 == null ? null : passportPage3,
    "personal_photo": personalPhoto == null ? null : personalPhoto,
    "ticket_copy": ticketCopy == null ? null : ticketCopy,
    "status": status == null ? null : status,
    "created": created == null ? null : created.toIso8601String(),
    "user": user == null ? null : user,
  };
}

class VisaVisa {
  VisaVisa({
    this.id,
    this.title,
    this.smallDisc,
    this.largeDisc,
    this.price,
    this.order,
    this.created,
  });

  final int id;
  final String title;
  final String smallDisc;
  final String largeDisc;
  final String price;
  final int order;
  final DateTime created;

  factory VisaVisa.fromJson(Map<String, dynamic> json) => VisaVisa(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    smallDisc: json["small_disc"] == null ? null : json["small_disc"],
    largeDisc: json["large_disc"] == null ? null : json["large_disc"],
    price: json["price"] == null ? null : json["price"],
    order: json["order"] == null ? null : json["order"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "small_disc": smallDisc == null ? null : smallDisc,
    "large_disc": largeDisc == null ? null : largeDisc,
    "price": price == null ? null : price,
    "order": order == null ? null : order,
    "created": created == null ? null : created.toIso8601String(),
  };
}
