// To parse this JSON data, do
//
//     final contactUsResponse = contactUsResponseFromJson(jsonString);

import 'dart:convert';

ContactUsResponse contactUsResponseFromJson(String str) => ContactUsResponse.fromJson(json.decode(str));

String contactUsResponseToJson(ContactUsResponse data) => json.encode(data.toJson());

class ContactUsResponse {
  ContactUsResponse({
    this.status,
    this.message,
    this.file,
  });

  final int status;
  final String message;
  final FileClass file;

  factory ContactUsResponse.fromJson(Map<String, dynamic> json) => ContactUsResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    file: json["file"] == null ? null : FileClass.fromJson(json["file"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "file": file == null ? null : file.toJson(),
  };
}

class FileClass {
  FileClass({
    this.id,
    this.subject,
    this.message,
    this.created,
    this.user,
  });

  final int id;
  final String subject;
  final String message;
  final DateTime created;
  final int user;

  factory FileClass.fromJson(Map<String, dynamic> json) => FileClass(
    id: json["id"] == null ? null : json["id"],
    subject: json["subject"] == null ? null : json["subject"],
    message: json["message"] == null ? null : json["message"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
    user: json["user"] == null ? null : json["user"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "subject": subject == null ? null : subject,
    "message": message == null ? null : message,
    "created": created == null ? null : created.toIso8601String(),
    "user": user == null ? null : user,
  };
}
