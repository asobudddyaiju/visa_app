// To parse this JSON data, do
//
//     final visaListResponse = visaListResponseFromJson(jsonString);

import 'dart:convert';

VisaListResponse visaListResponseFromJson(String str) => VisaListResponse.fromJson(json.decode(str));

String visaListResponseToJson(VisaListResponse data) => json.encode(data.toJson());

class VisaListResponse {
  VisaListResponse({
    this.status,
    this.message,
    this.visas,
  });

  final int status;
  final String message;
  final List<Visa> visas;

  factory VisaListResponse.fromJson(Map<String, dynamic> json) => VisaListResponse(
    status: json["status"] == null ? null : json["status"],
    message: json["message"] == null ? null : json["message"],
    visas: json["visas"] == null ? null : List<Visa>.from(json["visas"].map((x) => Visa.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "message": message == null ? null : message,
    "visas": visas == null ? null : List<dynamic>.from(visas.map((x) => x.toJson())),
  };
}

class Visa {
  Visa({
    this.id,
    this.title,
    this.smallDisc,
    this.largeDisc,
    this.price,
    this.order,
    this.created,
  });

  final int id;
  final String title;
  final String smallDisc;
  final String largeDisc;
  final String price;
  final int order;
  final DateTime created;

  factory Visa.fromJson(Map<String, dynamic> json) => Visa(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    smallDisc: json["small_disc"] == null ? null : json["small_disc"],
    largeDisc: json["large_disc"] == null ? null : json["large_disc"],
    price: json["price"] == null ? null : json["price"],
    order: json["order"] == null ? null : json["order"],
    created: json["created"] == null ? null : DateTime.parse(json["created"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "small_disc": smallDisc == null ? null : smallDisc,
    "large_disc": largeDisc == null ? null : largeDisc,
    "price": price == null ? null : price,
    "order": order == null ? null : order,
    "created": created == null ? null : created.toIso8601String(),
  };
}
