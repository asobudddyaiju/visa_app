// To parse this JSON data, do
//
//     final statusResponse = statusResponseFromJson(jsonString);

import 'dart:convert';

StatusResponse statusResponseFromJson(String str) =>
    StatusResponse.fromJson(json.decode(str));

String statusResponseToJson(StatusResponse data) => json.encode(data.toJson());

class StatusResponse {
  StatusResponse({
    this.available,
    this.visa,
  });

  final bool available;
  final Visa visa;

  factory StatusResponse.fromJson(Map<String, dynamic> json) => StatusResponse(
        available: json["available"] == null ? null : json["available"],
        visa: json["visa"] == null ? null : Visa.fromJson(json["visa"]),
      );

  Map<String, dynamic> toJson() => {
        "available": available == null ? null : available,
        "visa": visa == null ? null : visa.toJson(),
      };
}

class Visa {
  Visa({
    this.id,
    this.applicationId,
    this.name,
    this.email,
    this.countryCode,
    this.contactNumber,
    this.dateOfEntry,
    this.nationality,
    this.passportPage1,
    this.passportPage2,
    this.passportPage3,
    this.personalPhoto,
    this.ticketCopy,
    this.status,
    this.created,
  });

  final int id;
  final String applicationId;
  final String name;
  final String email;
  final String countryCode;
  final String contactNumber;
  final DateTime dateOfEntry;
  final String nationality;
  final String passportPage1;
  final String passportPage2;
  final dynamic passportPage3;
  final String personalPhoto;
  final dynamic ticketCopy;
  final int status;
  final DateTime created;

  factory Visa.fromJson(Map<String, dynamic> json) => Visa(
        id: json["id"] == null ? null : json["id"],
        applicationId:
            json["application_id"] == null ? null : json["application_id"],
        name: json["name"] == null ? null : json["name"],
        email: json["email"] == null ? null : json["email"],
        countryCode: json["country_code"] == null ? null : json["country_code"],
        contactNumber:
            json["contact_number"] == null ? null : json["contact_number"],
        dateOfEntry: json["date_of_entry"] == null
            ? null
            : DateTime.parse(json["date_of_entry"]),
        nationality: json["nationality"] == null ? null : json["nationality"],
        passportPage1:
            json["passport_page_1"] == null ? null : json["passport_page_1"],
        passportPage2:
            json["passport_page_2"] == null ? null : json["passport_page_2"],
        passportPage3: json["passport_page_3"],
        personalPhoto:
            json["personal_photo"] == null ? null : json["personal_photo"],
        ticketCopy: json["ticket_copy"],
        status: json["status"] == null ? null : json["status"],
        created:
            json["created"] == null ? null : DateTime.parse(json["created"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "application_id": applicationId == null ? null : applicationId,
        "name": name == null ? null : name,
        "email": email == null ? null : email,
        "country_code": countryCode == null ? null : countryCode,
        "contact_number": contactNumber == null ? null : contactNumber,
        "date_of_entry": dateOfEntry == null
            ? null
            : "${dateOfEntry.year.toString().padLeft(4, '0')}-${dateOfEntry.month.toString().padLeft(2, '0')}-${dateOfEntry.day.toString().padLeft(2, '0')}",
        "nationality": nationality == null ? null : nationality,
        "passport_page_1": passportPage1 == null ? null : passportPage1,
        "passport_page_2": passportPage2 == null ? null : passportPage2,
        "passport_page_3": passportPage3,
        "personal_photo": personalPhoto == null ? null : personalPhoto,
        "ticket_copy": ticketCopy,
        "status": status == null ? null : status,
        "created": created == null ? null : created.toIso8601String(),
      };
}
