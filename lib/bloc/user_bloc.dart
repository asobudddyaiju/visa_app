import 'dart:async';

import 'package:smart_travel/models/apply_visa_request.dart';
import 'package:smart_travel/models/apply_visa_response.dart';
import 'package:smart_travel/models/contact_us_request.dart';
import 'package:smart_travel/models/contact_us_response.dart';
import 'package:smart_travel/models/login_response_model.dart';
import 'package:smart_travel/models/my_vis_response.dart';
import 'package:smart_travel/models/payment_response_model.dart';
import 'package:smart_travel/models/state.dart';
import 'package:smart_travel/models/status_request.dart';
import 'package:smart_travel/models/status_response.dart';
import 'package:smart_travel/models/upload_image_request.dart';
import 'package:smart_travel/models/upload_image_response.dart';
import 'package:smart_travel/models/visa_list_model.dart';
import 'package:smart_travel/utils/constants.dart';
import 'package:smart_travel/utils/object_factory.dart';
import 'package:smart_travel/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class AppBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<VisaListResponse> _visaList =
      new StreamController<VisaListResponse>.broadcast();
  StreamController<ApplyVisaResponse> _applyVisa =
      new StreamController<ApplyVisaResponse>.broadcast();
  StreamController<UploadImageResponse> _uploadImage =
      new StreamController<UploadImageResponse>.broadcast();
  StreamController<ContactUsResponse> _contactUs =
      new StreamController<ContactUsResponse>.broadcast();
  StreamController<StatusResponse> _status =
      new StreamController<StatusResponse>.broadcast();
  StreamController<MyVisaResponse> _myVisa =
      new StreamController<MyVisaResponse>.broadcast();
  StreamController<LoginResponseModel> _login =
      new StreamController<LoginResponseModel>.broadcast();
  StreamController<PaymentResponseModel> _getPaymentUrlResponse =
      new StreamController<PaymentResponseModel>.broadcast();

  // ignore: close_sinks

//stream controller is broadcasting the  details

  Stream<VisaListResponse> get visaResponse => _visaList.stream;

  Stream<ApplyVisaResponse> get applyVisaResponse => _applyVisa.stream;

  Stream<UploadImageResponse> get uploadImageResponse => _uploadImage.stream;

  Stream<ContactUsResponse> get contactUsResponse => _contactUs.stream;

  Stream<StatusResponse> get statusCheckResponse => _status.stream;

  Stream<MyVisaResponse> get myVisaResponse => _myVisa.stream;

  Stream<LoginResponseModel> get loginResponse => _login.stream;

  Stream<PaymentResponseModel> get getPaymentUrlResponse =>
      _getPaymentUrlResponse.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  visaList() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.visaList();

    if (state is SuccessState) {
      loadingSink.add(false);
      _visaList.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _visaList.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  login({String uuid}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.login(uuid: uuid);

    if (state is SuccessState) {
      loadingSink.add(false);
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _login.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  myVisa() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.myVisa();

    if (state is SuccessState) {
      loadingSink.add(false);
      _myVisa.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _myVisa.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  applyVisaList({ApplyVisaRequest applyVisaRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.applyVisa(
        applyVisaRequest: ApplyVisaRequest(
            apiKey: applyVisaRequest.apiKey,
            visa: applyVisaRequest.visa,
            name: applyVisaRequest.name,
            email: applyVisaRequest.email,
            countryCode: applyVisaRequest.countryCode,
            contactNumber: applyVisaRequest.contactNumber,
            dateOfEntry: applyVisaRequest.dateOfEntry,
            nationality: applyVisaRequest.nationality,
            passportPage1: applyVisaRequest.passportPage1,
            passportPage2: applyVisaRequest.passportPage2,
            passportPage3: applyVisaRequest.passportPage3,
            personalPhoto: applyVisaRequest.personalPhoto,
            ticketCopy: applyVisaRequest.ticketCopy));

    if (state is SuccessState) {
      loadingSink.add(false);
      _applyVisa.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _applyVisa.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  uploadImage({UploadImageRequest uploadImageRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.uploadImage(
        uploadImageRequest: UploadImageRequest(
            // apiKey: uploadImageRequest.apiKey,
            imageFile: uploadImageRequest.imageFile));

    if (state is SuccessState) {
      loadingSink.add(false);
      _uploadImage.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _uploadImage.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  contactUs({ContactUsRequest contactUsRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.contactUs(
        contactUsRequest: ContactUsRequest(
            subject: contactUsRequest.subject,
            message: contactUsRequest.message));

    if (state is SuccessState) {
      loadingSink.add(false);
      _contactUs.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _contactUs.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  statusCheck({StatusRequest statusRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.statusCheck(
        statusRequest: StatusRequest(
            apiKey: statusRequest.apiKey,
            applicationId: statusRequest.applicationId));

    if (state is SuccessState) {
      loadingSink.add(false);
      _status.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _status.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getPaymentUrl({String name, String mobileNo, String amount}) async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.getPaymentUrl(name, mobileNo, amount);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getPaymentUrlResponse.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getPaymentUrlResponse.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _visaList?.close();
    _applyVisa?.close();
    _uploadImage?.close();
    _contactUs?.close();
    _status?.close();
    _myVisa?.close();
    _login?.close();
    _getPaymentUrlResponse?.close();
  }
}

AppBloc appBlocSingle = AppBloc();
