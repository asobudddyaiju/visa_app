class Urls{
  static final baseUrl = "https://affiliates.smarttravels.ae/";
  static final visaListUrl = baseUrl+"api/v1/visas/";
  static final applyForVisaUrl=baseUrl+"api/v1/visa/application/";
  static final uploadImageUrl=baseUrl+ "api/v1/file/upload/";
  static final contactUsUrl=baseUrl+ "api/v1/contact/";
  static final statusUrl=baseUrl+ "api/v1/visa/application/status/";
  static final myVisaUrl=baseUrl+ "api/v1/my/visas/";
  static final loginUrl=baseUrl+ "api/v1/google/login/";
  static final paymentBaseUrl= "http://b2b.smarttravels.ae/apidispatch.jsp?";
}