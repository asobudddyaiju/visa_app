import 'dart:ui';

class Constants {
  ///error
  static const String SOME_ERROR_OCCURRED = "Some error occurred.";
  static const String LOGIN_FIELD_EMPTY =
      "Email, password fields are mandatory.";

  ///validators
  static const String EMAIL_NOT_VALID = "Email is not valid";
  static const String USERNAME_NOT_VALID = "Username is not valid";
  static const String PASSWORD_LENGTH =
      "Password length should be greater than 5 chars.";
  static const String INVALID_MOBILE_NUM = "Invalid mobile number";
  static const String INVALID_NAME = "Invalid name";

  ///hive
  static const String BOX_NAME = "user box";

  static List<Color> kitGradients = [
    Color(0xFFE5E5E5),
    Color(0xFFFFFFFF),
    Color(0xFF6C6C6C),
    Color(0xFF016BA6),
    Color(0xFF5E5E5E),
    Color(0xFFF0EFEF), //5
    Color(0xFFC4C4C4),
    Color(0xFFEEEEEE),
    Color(0xFF0172b1),
    Color(0xFFCECECE),
    Color(0xFF5E5E5E),//10
    Color(0xFFD7D7D7),
    Color(0xFFEEEEEE),
    Color(0xFF929292),
    Color(0xFFC50000),
    Color(0xFF508F00),//15
    Color(0xFFDF8600),
  ];
}
